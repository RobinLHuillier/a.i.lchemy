class MarketingButton extends Button {
    constructor(
        x,y,w,h,ctx,lineWidth,colorStroke,colorFill,colorText,sizeText,text,xText=4,title,colorHover,colorFillActive,colorStrokeActive,colorTextActive,visible,activable=true,
        type, cost, speed, flavorText, quantity, active=false,
    ) {
        super(x,y,w,h,ctx,lineWidth,colorStroke,colorFill,colorText,sizeText,text,xText=4,title,colorHover,colorFillActive,colorStrokeActive,colorTextActive,visible,activable=true);

        this.type = type;
        this.progress = type === "progress";
        this.instant = type === "instant";
        this.perma = type === "perma";
        this.cost = cost;
        this.speed = speed;
        this.advancement = 0;
        this.flavorText = flavorText;
        this.quantity = quantity;
        this.active = active;
    }

    click(mouseX, mouseY) {
        const active = this.active;
        const result = super.click(mouseX, mouseY);
        if (result.length > 0 && !active && !this.perma) {
            if (currencyHandler.howMuchMoney() >= this.cost) {
                currencyHandler.payMoney(this.cost);
                if (this.instant) {
                    save.people.weakCouples += this.quantity;
                }
            } else {
                this.active = false;
            }
        } else if (result.length > 0 && active && this.perma) {
            this.active = false;
        }
    }

    advance() {
        if (!this.active) {
            return;
        }

        if (this.perma) {
            if (currencyHandler.howMuchMoney() < this.cost) {
                this.active = false;
                return;
            }
            currencyHandler.payMoney(this.cost);
        }

        if (this.progress) {
            cardHandler.generateNewCards(this.quantity);
        }

        this.advancement += this.speed;
        if (this.advancement >= 100) {
            this.advancement = 0;
            this.active = this.perma;

            if (this.perma) {
                save.people.potentialPool += this.quantity;
            }
        }
    }

    draw(mouseX, mouseY) {
        if (!this.visible) {
            return;
        }

        super.draw(mouseX, mouseY);

        this.ctx.font = "20px Arial";
        this.ctx.fillStyle = this.active ? this.colorTextActive : this.colorText;
        this.ctx.fillText(this.flavorText, this.x+5, this.y+60, this.w-10);
        if (this.instant) {
            this.ctx.fillText("Cost: "+formatNumber(this.cost), this.x+5, this.y+85, this.w-10);
            this.ctx.fillText("Cooldown: "+ ticksToTime(100/this.speed), this.x+145, this.y+85, this.w-150);
            this.ctx.fillText("Weak couples + "+formatNumber(this.quantity), this.x+5, this.y+110, this.w-10);
        }
        if (this.progress) {
            this.ctx.fillText("Cost: "+formatNumber(this.cost), this.x+5, this.y+85, 140);
            this.ctx.fillText("Duration: "+ticksToTime(100/this.speed), this.x+145, this.y+85, this.w-150);
            this.ctx.fillText(formatNumber(this.quantity)+" customers/tick", this.x+5, this.y+110, this.w-10);
        }
        if (this.perma) {
            this.ctx.fillText("Cost: "+formatNumber(tickPerSec*this.cost)+"/s", this.x+5, this.y+85, this.w-10);
            this.ctx.fillText("Proc every "+ticksToTime(100/this.speed), this.x+145, this.y+85, this.w-150);
            this.ctx.fillText("Potential pool + "+formatNumber(this.quantity), this.x+5, this.y+110, this.w-10);
        }

        if (this.advancement > 0) {
            this.ctx.lineWidth = 20;
            this.ctx.lineCap = "round";
            this.ctx.strokeStyle = this.active ? pink : night;
            this.ctx.beginPath();
            this.ctx.moveTo(this.x+15, this.y + 140);
            this.ctx.lineTo(this.x+15 + (this.advancement/100)*(this.w-30), this.y + 140);
            this.ctx.stroke();
        }
    }
}