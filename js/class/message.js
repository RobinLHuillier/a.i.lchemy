class Message {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.text = "";
        this.color = "";
        this.fade = 1;
        this.textSizeMult = 1;
    }
    reset(x, y, text, color, textSizeMult = 1) {
        this.x = x;
        this.y = y;
        this.text = text;
        this.color = color;
        this.fade = 0.99;
        this.textSizeMult = textSizeMult;
    }
    advance() {
        this.fade -= (1-this.fade)*0.2;
        this.y -= 1;
        return this.fade <= 0.1;
    }
    draw() {
        ctxMessage.fillStyle = addFade(this.color,this.fade);
        ctxMessage.font = (20*this.textSizeMult).toString() + "px Arial";
        ctxMessage.fillText(this.text, this.x, this.y);
    }
}