class Card {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.sizeX = 1;
        this.sizeY = 1;
        this.sex = "";
        this.fade = 0;
        this.name = "";
        this.sexuality = "";
        this.age = 0;
        this.ageRange = [];
        this.typeRelation = "";
        this.like = [];
        this.dislike = [];
        
        this.discovery = 0;
        this.discoverRemaining = [];
        this.showAge = false;
        this.showGender = false;
        this.showSexuality = false;
        this.showAgeRange = false;
        this.showTypeRelation = false;
        this.showLikes = [];
        this.showDislikes = [];

        this.degen = 1;
        this.hover = false;
        this.display = "primary";
        this.newInfo = false;
        this.simplified = false;
        this.ultraSimplified = false;
    }

    reset(x, y, sizeX, sizeY, simplified=false, ultraSimplified=false, infoQuantity=1) {
        this.x = x;
        this.y = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.fade = 0;

        if (!simplified) {
            const instant = save.cards.instantDiscovery;
            this.discovery = 0;
            this.randomizeFeatures();
            this.showAge = instant;
            this.showGender = instant;
            this.showLikes = [false, false, false];
            this.showDislikes = [false, false, false];
            if (infoQuantity === 1 && !instant) {
                this.discoverRemaining = ["age", "gender"];
            }
            if (infoQuantity > 1) {
                this.showSexuality = instant;
                if (!instant) {
                    this.discoverRemaining = ["age", "gender", "sexuality"];
                }
            } else {
                this.showSexuality = false;
            }
            if (infoQuantity > 2) {
                this.showAgeRange = instant;
                if (!instant) {
                    this.discoverRemaining = ["age", "gender", "sexuality", "ageRange"];
                }
            } else {
                this.showAgeRange = false;
            }
            if (infoQuantity > 3) {
                this.showTypeRelation = instant;
                if (!instant) {
                    this.discoverRemaining = ["age", "gender", "sexuality", "ageRange", "typeRelation"];
                    for (let i=0; i<this.like.length; i++) {
                        this.discoverRemaining.push("like"+i.toString());
                    }
                    for (let i=0; i<this.dislike.length; i++) {
                        this.discoverRemaining.push("dislike"+i.toString());
                    }
                } else {
                    for (let i=0; i<this.like.length; i++) {
                        this.showLikes[i] = true;
                    }
                    for (let i=0; i<this.dislike.length; i++) {
                        this.showDislikes[i] = true;
                    }
                    this.newInfo = true;    
                }
            } else {
                this.showTypeRelation = false;
            }
            
            if (instant) {
                this.discoverRemaining = [];
            }
        }
        if (simplified && !ultraSimplified) {
            this.randomizeName();
        }

        this.degen = 1;
        this.simplified = simplified;
        this.ultraSimplified = ultraSimplified;
    }

    randomizeName() {
        this.name = randFirstName(randGender()) + " " + randLastName();
    }

    randomizeFeatures() {
        this.gender = randGender();
        this.name = randFirstName(this.gender) + " " + randLastName();
        this.sexuality = randSexuality(this.gender);
        this.age = randAge();
        this.ageRange = randAgeRange(this.age);
        this.typeRelation = randTypeRelation();
        this.like = randLikeDislike();
        this.dislike = randLikeDislike(this.like);
    }

    changeSize(sizeX, sizeY, realX, realY, simplified, ultraSimplified) {
        this.x = realX;
        this.y = realY;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.newInfo = true;
        this.simplified = simplified;
        this.ultraSimplified = ultraSimplified;
        if (simplified || ultraSimplified) {
            this.discoverRemaining = [];
        }
    }

    discover() {
        if (this.discoverRemaining.length === 0) {
            console.error("[Card.discover] no more discoveries when trying to discover");
            return;
        }
        const disco = randElem(this.discoverRemaining);
        switch (disco) {
            case "age":
                this.showAge = true;
                break;
            case "gender":
                this.showGender = true;
                break;
            case "sexuality":
                this.showSexuality = true;
                break;
            case "ageRange":
                this.showAgeRange = true;
                break;
            case "typeRelation":
                this.showTypeRelation = true;
                break;
            case "like0":
                this.showLikes[0] = true;
                break;
            case "like1":
                this.showLikes[1] = true;
                break;
            case "like2":
                this.showLikes[2] = true;
                break;
            case "dislike0":
                this.showDislikes[0] = true;
                break;
            case "dislike1":
                this.showDislikes[1] = true;
                break;
            case "dislike2":
                this.showDislikes[2] = true;
                break;   
            default:
                break;
        }
        this.newInfo = true;
        delElem(this.discoverRemaining, disco);
    }

    isAllDiscovered() {
        return this.discoverRemaining.length === 0
    }

    advanceDiscovery(multiplier = 1) {
        if (this.isAllDiscovered()) {
            this.degen -= save.cards.degenSpeed;
            if (this.degen < 0) {
                this.degen = 0;
                return this.fade >= 1;
            }
            return false;
        }
        this.discovery += save.cards.discoverySpeed * multiplier;    
        if (this.discovery >= 1) {
            this.discovery = 0;
            this.discover();
        }
        return false;
    }

    fadeToBlack() {
        const speedFade = 0.1;
        this.fade -= speedFade;
        return this.fade <= -speedFade;
    }

    fadeToWhite() {
        const speedFade = 0.1;
        this.fade += speedFade;
        return this.fade >= 1;
    }

    formatAgeRange() {
        if (this.ageRange[0] === 65) {
            return "65+";
        }
        return this.ageRange[0].toString() + "-" + this.ageRange[1].toString();
    }

    resetDraw() {
        ctxCards.clearRect(this.x-2, this.y-2, this.sizeX+4, this.sizeY+4);
    }

    draw(highlight = false) {
        const round = 7;
        const fontSizeBig = Math.floor(this.sizeX/8);
        const textOffset = 4;
        const textLine = fontSizeBig;

        if (this.hover !== highlight || (this.fade > 0 && this.fade < 1) || this.newInfo) {
            this.hover = highlight;
            this.newInfo = true;

            ctxCards.clearRect(this.x-2, this.y-2, this.sizeX+4, this.sizeY+4);
            
            // card tile
            ctxCards.strokeStyle = addFade(pink, this.fade);
            if (highlight) {
                ctxCards.fillStyle = addFade(gold, this.fade);
            } else {
                ctxCards.fillStyle = addFade(yellow, this.fade);
            }
            if (this.discoverRemaining.length === 0) {
                ctxCards.lineWidth = 4; 
                ctxCards.strokeStyle = addFade(purple, this.fade);
            } else {
                ctxCards.lineWidth = 2; 
            }

            ctxCards.beginPath();
            ctxCards.moveTo(this.x+round, this.y);
            ctxCards.lineTo(this.x+this.sizeX-round, this.y);
            ctxCards.arcTo(this.x+this.sizeX,this.y, this.x+this.sizeX,this.y+round, round);
            ctxCards.lineTo(this.x+this.sizeX,this.y+this.sizeY-round);
            ctxCards.arcTo(this.x+this.sizeX,this.y+this.sizeY, this.x+this.sizeX-round, this.y+this.sizeY, round);
            ctxCards.lineTo(this.x+round,this.y+this.sizeY);
            ctxCards.arcTo(this.x,this.y+this.sizeY,this.x,this.y+this.sizeY-round, round);
            ctxCards.lineTo(this.x,this.y+round);
            ctxCards.arcTo(this.x,this.y,this.x+round,this.y, round);
            ctxCards.fill();
            ctxCards.stroke();

            if (!this.ultraSimplified) {
                // name
                ctxCards.font = fontSizeBig.toString() + "px Arial";
                if (highlight) {
                    ctxCards.fillStyle = addFade(night, this.fade);
                } else {
                    ctxCards.fillStyle = addFade(blue, this.fade);
                }
                ctxCards.fillText(this.name, this.x+textOffset, this.y+textLine+textOffset, this.sizeX-2*textOffset);
            }
        }
        
        if (!this.simplified) {
            // discovery-degen line
            ctxCards.lineWidth = Math.floor(this.sizeY/15);
            ctxCards.lineCap = "round";
            // clear it
            if (highlight) {
                ctxCards.strokeStyle = addFade(gold, this.fade);
            } else {
                ctxCards.strokeStyle = addFade(yellow, this.fade);
            }
            ctxCards.beginPath();
            ctxCards.moveTo(this.x+3*textOffset, this.y+2*textLine);
            ctxCards.lineTo(this.x+3*textOffset+this.sizeX-6*textOffset, this.y+2*textLine);
            ctxCards.stroke();
            // draw it
            ctxCards.beginPath();
            ctxCards.moveTo(this.x+3*textOffset, this.y+2*textLine);
            if (!this.isAllDiscovered()) {
                if (highlight) {
                    ctxCards.strokeStyle = addFade(night, this.fade);
                } else {
                    ctxCards.strokeStyle = addFade(blue, this.fade);
                }
                ctxCards.lineTo(this.x+3*textOffset+this.discovery*(this.sizeX-6*textOffset), this.y+2*textLine);
            } else {
                ctxCards.strokeStyle = addFade(pink, this.fade);
                ctxCards.lineTo(this.x+3*textOffset+this.degen*(this.sizeX-6*textOffset), this.y+2*textLine);
            }
            ctxCards.stroke();
        }

        if (this.simplified && this.newInfo) {
            this.newInfo = false;
            return;
        }
    }

    displayPrimaryInfos() {
        if (this.simplified) {
            return;
        }

        if (this.display = "primary" && !this.newInfo) {
            return;
        }
        this.display = "primary";
        this.newInfo = false;

        const fontSizeBig = Math.floor(this.sizeX/8);
        const fontSizeMedium = Math.floor(this.sizeX/8);
        const textOffset = 4;
        const textLine = fontSizeBig;
        ctxCards.font = fontSizeMedium.toString() + "px Arial";
        if (this.showAge) {
            ctxCards.fillText("Age: "+this.age.toString(), this.x+textOffset, this.y+3.5*textLine, this.sizeX-2*textOffset);
        }
        if (this.showGender) {
            ctxCards.fillText("Gender: "+this.gender, this.x+textOffset, this.y+4.5*textLine, this.sizeX-2*textOffset);
        }
        if (this.showSexuality) {
            ctxCards.fillText("Seek: "+this.sexuality, this.x+textOffset, this.y+5.5*textLine, this.sizeX-2*textOffset);
        }
        if (this.showAgeRange) {
            ctxCards.fillText("Seek age: "+this.formatAgeRange(), this.x+textOffset, this.y+6.5*textLine, this.sizeX-2*textOffset);
        }
    }

    displaySecondaryInfos() {
        if (this.simplified) {
            return;
        }

        if (this.display = "secondary" && !this.newInfo) {
            return;
        }
        this.display = "secondary";
        this.newInfo = false;

        const fontSizeBig = Math.floor(this.sizeX/8);
        const fontSizeLittle = Math.floor(this.sizeX/10);
        const textOffset = 4;
        const textLine = fontSizeBig;
        ctxCards.font = fontSizeLittle.toString() + "px Arial";
        if (this.showTypeRelation) {
            ctxCards.fillText(this.typeRelation + " relationship", this.x+textOffset, this.y+3.5*textLine, this.sizeX-2*textOffset);
        }

        if (!this.showLikes[0] && (this.showLikes[1] || this.showLikes[2])) {
            ctxCards.fillText("Like: ", this.x+textOffset, this.y+4.5*textLine, this.sizeX-2*textOffset);
        } else if (this.showLikes[0]) {
            ctxCards.fillText("Like: "+this.like[0], this.x+textOffset, this.y+4.5*textLine, this.sizeX-2*textOffset);
        }
        if (this.showLikes[1] && (this.like.length === 2 || (this.like.length === 3 && !this.showLikes[2]))) {
            ctxCards.fillText(this.like[1], this.x+4*textOffset, this.y+5.5*textLine, this.sizeX-5*textOffset);
        } else if (this.like.length === 3) {
            if (!this.showLikes[1] && this.showLikes[2]) {
                ctxCards.fillText(this.like[2], this.x+4*textOffset, this.y+5.5*textLine, this.sizeX-5*textOffset);
            } else if (this.showLikes[1] && this.showLikes[2]) {
                ctxCards.fillText(this.like[1]+","+this.like[2], this.x+4*textOffset, this.y+5.5*textLine, this.sizeX-5*textOffset);
            }
        }

        if (!this.showDislikes[0] && (this.showDislikes[1] || this.showDislikes[2])) {
            ctxCards.fillText("Dislike: ", this.x+textOffset, this.y+6.5*textLine, this.sizeX-2*textOffset);
        } else if (this.showDislikes[0]) {
            ctxCards.fillText("Dislike: "+this.dislike[0], this.x+textOffset, this.y+6.5*textLine, this.sizeX-2*textOffset);
        }
        if (this.showDislikes[1] && (this.dislike.length === 2 || (this.dislike.length === 3 && !this.showDislikes[2]))) {
            ctxCards.fillText(this.dislike[1], this.x+4*textOffset, this.y+7.5*textLine, this.sizeX-5*textOffset);
        } else if (this.dislike.length === 3) {
            if (!this.showDislikes[1] && this.showDislikes[2]) {
                ctxCards.fillText(this.dislike[2], this.x+4*textOffset, this.y+7.5*textLine, this.sizeX-5*textOffset);
            } else if (this.showDislikes[1] && this.showDislikes[2]) {
                ctxCards.fillText(this.dislike[1]+","+this.dislike[2], this.x+4*textOffset, this.y+7.5*textLine, this.sizeX-5*textOffset);
            }
        }
    }
}