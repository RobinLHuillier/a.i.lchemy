class Upgrade {
    constructor(title, tooltip, cost, advancement, bought, effect, moneyPerTick) {
        this.title = title;
        this.tooltip = [...tooltip];
        this.cost = cost;
        this.advancement = advancement;
        this.bought = bought;
        this.effect = effect;
        this.moneyPerTick = moneyPerTick;
        this.fadeTooltip = 0;
    }

    serialize() {
        return [this.title, [...this.tooltip], this.cost, this.advancement, this.bought, this.effect, this.moneyPerTick];
    }

    activate(moneyPerTick) {
        this.moneyPerTick = moneyPerTick;
    }

    advance(quantity = undefined) {
        if (undefined !== quantity) {
            this.advancement += quantity;
        } else {
            this.advancement += this.moneyPerTick;
        }
        if (this.advancement > this.cost) {
            const remain = this.advancement - this.cost;
            this.advancement = this.cost;
            return remain;
        }
        return 0;
    }

    fadeInTooltip() {
        this.fadeTooltip += 0.25;
        if (this.fadeTooltip >= 1) {
            this.fadeTooltip = 1;
        }
    }

    fadeOutTooltip() {
        if (this.fadeTooltip === 0) {
            return;
        }
        this.fadeTooltip -= 0.25;
        if (this.fadeTooltip < 0) {
            this.fadeTooltip = 0;
        }
    }

    formatTimeRemaining() {
        if (this.moneyPerTick <= 0.001) {
            return infSymbol;
        }
        const tick = (this.cost-this.advancement)/this.moneyPerTick;
        const sec = tick/tickPerSec;
        const min = sec/60;
        const hours = min/60;
        const days = hours/24;
        if (min < 1) {
            return (Math.floor(sec*10)/10).toString()+"s";
        }
        if (hours < 1) {
            return (Math.floor(min*100)/100).toString()+"m";
        }
        if (days < 1) {
            return (Math.floor(hours*100)/100).toString()+"h";
        }
        return (Math.floor(days*100)/100).toString()+"d";
    }

    formatCostRemaining() {
        return formatNumber(Math.round(this.cost-this.advancement));
    }

    lengthiestTooltip() {
        let maxi = 0;
        for (let i=0; i<this.tooltip.length; i++) {
            if (this.tooltip[i].length > maxi) {
                maxi = this.tooltip[i].length;
            }
        }
        return maxi;
    }

    drawTooltip(x, y, w, h) {
        const len = this.lengthiestTooltip();
        if (len === 0) {
            return;
        }
        const sx = x + w + 2;
        const sy = y + Math.round(h/2);
        const sw = 10*len;
        const sh = h-18;
        const dh = Math.round(sh/2);
        ctxMenu.lineWidth = 1;
        ctxMenu.strokeStyle = addFade(pink, this.fadeTooltip);
        ctxMenu.fillStyle = addFade(gold, this.fadeTooltip);
        ctxMenu.beginPath();
        ctxMenu.moveTo(sx, sy);
        ctxMenu.lineTo(sx + 5, sy - 5);
        ctxMenu.lineTo(sx + 5, sy - dh);
        ctxMenu.lineTo(sx+5+sw, sy-dh);
        ctxMenu.lineTo(sx+5+sw, sy+dh);
        ctxMenu.lineTo(sx+5, sy+dh);
        ctxMenu.lineTo(sx+5, sy+5);
        ctxMenu.lineTo(sx, sy);
        ctxMenu.closePath();
        ctxMenu.fill();
        ctxMenu.stroke();

        ctxMenu.font = "20px Arial";
        ctxMenu.fillStyle = addFade(blue, this.fadeTooltip);
        for (let j=0; j<this.tooltip.length; j++) {
            ctxMenu.fillText(this.tooltip[j], sx + 10, sy-dh+25*(j+1), sw-10);
        }
    }

    draw(x, y, w, h, hover=false) {
        ctxMenu.lineWidth = 1;
        if (this.moneyPerTick > 0) {
            ctxMenu.lineWidth = 3;
        }
        roundRect(ctxMenu, x, y, w, h, hover ? night : blue, hover ? gold : yellow, 5);

        ctxMenu.fillStyle = purple;
        ctxMenu.font = "20px Arial";
        ctxMenu.fillText(this.title + ":", x+5, y+30, 150);
        ctxMenu.fillStyle = blue;
        ctxMenu.fillText("$ " + this.formatCostRemaining(), x+160, y+30, 60);
        ctxMenu.fillText("("+this.formatTimeRemaining()+")", x+225, y+30, 70);

        if (this.moneyPerTick > 0) {
            ctxMenu.strokeStyle = blue;
        } else {
            ctxMenu.strokeStyle = pink;
        }
        ctxMenu.lineCap = "round";
        ctxMenu.lineWidth = 15;
        ctxMenu.beginPath();
        ctxMenu.moveTo(x+10, y+60);
        ctxMenu.lineTo(x+10+(w-20)*(this.cost-this.advancement)/this.cost, y+60);
        ctxMenu.stroke();

        if (hover) {
            this.fadeInTooltip();
        } else {
            this.fadeOutTooltip();
        }
        if (this.fadeTooltip > 0) {
            this.drawTooltip(x,y,w,h);
        }
    }
}