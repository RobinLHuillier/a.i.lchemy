var gameHandler;
var cardHandler;
var messageHandler;
var menuHandler;
var currencyHandler;
var upgradeHandler;
var saveHandler;
var statHandler;
var marketingHandler;
var aiHandler;

function initHandlers() {
    cardHandler = new CardHandler();
    messageHandler = new MessageHandler();
    menuHandler = new MenuHandler();
    currencyHandler = new CurrencyHandler();
    upgradeHandler = new UpgradeHandler();
    saveHandler = new SaveHandler();
    statHandler = new StatHandler();
    marketingHandler = new MarketingHandler();
    aiHandler = new AiHandler();
}

document.addEventListener('mousemove', e => {
    canvasPosition = canvasFront.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown() {
    gameHandler.mouseDown();
}

function mouseUp() {
    gameHandler.mouseUp();
}

function animate(timestamp) {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        if(!gameHandler.amIWorking) {
            // console.time("core");
            gameHandler.core();
            // console.timeEnd("core");
            timer += timestamp - lastTime;
            lastTime = timestamp;
        }
    }
    window.requestAnimationFrame(animate);
}

window.onload = function() {
    ctxFront.clearRect(0,0,canvasFront.width,canvasFront.height);
    ctxBackground.fillStyle = white;
    ctxBackground.fillRect(0,0,canvasBackground.width,canvasBackground.height);
    initHandlers();
    gameHandler = new GameHandler();
    gameHandler.initialize().then(animate(performance.now()));
}