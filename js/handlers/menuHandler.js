class MenuHandler {
    constructor() {
        this.buttons = {
            options: [],
            tabs: [],
            TabResearch: [],
        };
        this.frontMenu = "";
    }

    initialize() {
        this.createButtons();
        this.firstDraw();
    }

    firstDraw() {
        const ctx = ctxMenuStatic;
        ctx.clearRect(0,0,canvasFront.width,canvasFront.height);
        // title
        ctx.font = "55px Arial";
        ctx.fillStyle = gold;
        ctx.strokeStyle = night;
        ctx.lineWidth = 1;
        ctx.fillText("A.I.lchemy", 10, 60);
        ctx.strokeText("A.I.lchemy", 10, 60);
        // banner
        strokeMenuRect(save.grid.offsetX-6, 4, save.grid.sizeX+12, save.grid.offsetY-14, ctxMenuStatic);
        // main
        strokeMenuRect(save.grid.offsetX-6, save.grid.offsetY-6, save.grid.sizeX+12, save.grid.sizeY+12, ctxMenuStatic);
        // left tabs
        strokeMenuRect(4, save.grid.offsetY-6, save.grid.offsetX-14, save.grid.sizeY+12, ctxMenuStatic);
    }

    createButtons() {
        // options
        this.buttons.options.push(new Button(1140, 20, 40, 50, ctxFront, 2, blue, white, night, 30, "⛌", 5, "close", gold, night, night, night, false, false));
        this.buttons.options.push(new Button(280, 20, 40, 50, ctxMenu, 2, blue, white, night, 30, "⚙", 7, "Options", yellow, night, night, night, true, false));
        this.buttons.options.push(new Button(330, 20, 40, 50, ctxMenu, 2, blue, white, blue, 30, "?", 11, "Help", yellow, night, night, night, true, false));
        this.buttons.options.push(new Button(400, 550, 400, 65, ctxFront, 4, blue, white, night, 40, "☠ Reset save ☠", 45, "EraseSave", gold, night, night, night, false, false));
        this.buttons.options.push(new Button(400, 550, 400, 65, ctxFront, 4, blue, white, night, 40, "Are you sure ?", 80, "EraseSaveConfirm", gold, night, night, night, false, false));

        // tab titles
        const width = save.grid.offsetX-32;
        const offsetX = 14;
        const offsetY = save.grid.offsetY+4;
        const titleWidth = width/4;
        this.buttons.tabs.push(new Button(offsetX, offsetY, titleWidth-4, 40, ctxMenu, 2, pink, white, night, 20, "Marketing",undefined, "TabMarketing", yellow, blue, purple, yellow, save.marketing.unlocked));
        this.buttons.tabs.push(new Button(offsetX+titleWidth, offsetY, titleWidth-4, 40, ctxMenu, 2, pink, white, night, 20, "R&D", 22, "TabResearch", yellow, blue, purple, yellow, true));
        this.buttons.tabs.push(new Button(offsetX+2*titleWidth, offsetY, titleWidth-4, 40, ctxMenu, 2, pink, white, night, 20, "A.I.",28, "TabAI", yellow, blue, purple, yellow, true));
        this.buttons.tabs[2].active = true;
        this.buttons.tabs.push(new Button(offsetX+3*titleWidth, offsetY, titleWidth-4, 40, ctxMenu, 2, pink, white, night, 20, "TBD",21, "TabTBD", yellow, blue, purple, yellow, false));

        // research -10 -1 0 +1 +10
        const offPlusY = offsetY + 90;
        this.buttons.TabResearch.push(new Button(30, offPlusY, 40, 30, ctxMenu, 1, pink, yellow, night, 15, "-100", 5, "Research-100", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(80, offPlusY, 40, 30, ctxMenu, 1, pink, yellow, night, 15, "-10", 8, "Research-10", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(130, offPlusY, 30, 30, ctxMenu, 1, pink, yellow, night, 15, "-1", 8, "Research-1", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(170, offPlusY, 30, 30, ctxMenu, 1, pink, yellow, night, 15, "0", 11, "Research0", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(210, offPlusY, 30, 30, ctxMenu, 1, pink, yellow, night, 15, "+1", 6, "Research+1", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(250, offPlusY, 40, 30, ctxMenu, 1, pink, yellow, night, 15, "+10", 7, "Research+10", gold, blue, purple, yellow, false, false));
        this.buttons.TabResearch.push(new Button(300, offPlusY, 40, 30, ctxMenu, 1, pink, yellow, night, 15, "+100", 3, "Research+100", gold, blue, purple, yellow, false, false));
    }

    unlock(title) {
        this.buttons.tabs.forEach(button => {
            if (button.title === title) {
                button.setVisible();
            }
        });
    }

    win() {
        this.frontMenu = "win";
        this.buttons.options[0].setVisible();
    }

    click() {
        let clickOnMenu = false;

        for (let i=0; i<this.buttons.options.length; i++) {
            const button = this.buttons.options[i];
            const result = button.click(mouse.x, mouse.y);
            if (result.length > 0) {
                if (result === "close") {
                    this.frontMenu = "close";
                    this.buttons.options[0].setInvisible();
                    this.buttons.options[3].setInvisible();
                    this.buttons.options[4].setInvisible();
                } else {
                    if (result === "Options" || result === "Help") {
                        this.frontMenu = result;
                        this.buttons.options[0].setVisible();
                        if (result === "Options") {
                            this.buttons.options[3].setVisible();
                        }
                    } else if (result === "EraseSave") {
                        this.buttons.options[3].setInvisible();
                        this.buttons.options[4].setVisible();
                    } else if (result === "EraseSaveConfirm") {
                        saveHandler.resetSave();
                        resetSave();
                        gameHandler.reinit();
                        this.frontMenu = "close";
                        this.buttons.options[0].setInvisible();
                        this.buttons.options[3].setInvisible();
                        this.buttons.options[4].setInvisible();
                    }
                }
                return true;
            }
        }

        if (clickOnMenu || this.frontMenu.length > 0) {
            return true;
        }

        for (let i=0; i<this.buttons.tabs.length; i++) {
            const result = this.buttons.tabs[i].click(mouse.x, mouse.y);
            if (result.length > 0) {
                clickOnMenu = true;
                this.buttons.tabs.map(tab => {
                    if (tab.title !== result) tab.unactive();
                    if (this.buttons[tab.title] !== undefined) {
                        this.buttons[tab.title].map(button => button.setInvisible())
                    }
                });
                if (this.buttons[result] !== undefined) {
                    this.buttons[result].map(button => button.setVisible())
                }
            }
        }

        if (clickOnMenu) {
            return clickOnMenu;
        }

        const activeTab = this.getActiveTabTitle();
        if (undefined !== this.buttons[activeTab]) {
            this.buttons[activeTab].forEach(button => {
                const result = button.click(mouse.x, mouse.y);
                if (result.length > 0) {
                    upgradeHandler.changeResearchAmount(result);
                    clickOnMenu = true;
                }
            });
        }

        if (clickOnMenu) {
            return clickOnMenu;
        }

        if (activeTab === "TabResearch") {
            clickOnMenu = upgradeHandler.click();
        }

        if (clickOnMenu) {
            return clickOnMenu;
        }

        if (activeTab === "TabMarketing") {
            clickOnMenu = marketingHandler.click();
        }

        return clickOnMenu;
    }

    getActiveTabTitle() {
        const result = this.buttons.tabs.filter(button => button.active);
        if (result.length === 0) {
            console.error("[MenuHandler.getActiveTabTitle] no active tab title");
            return "";
        }
        return result[0].title;
    }

    drawMenu() {
        if (this.frontMenu.length === 0) {
            return;
        }
        ctxFront.clearRect(0,0,canvasFront.width,canvasFront.height);
        if (this.frontMenu === "close") {
            this.frontMenu = "";
            return;
        }

        let fade = 0.8;
        if (this.frontMenu === "Help") {
            fade = 0.8;
        }
        strokeMenuRect(0,0,canvasFront.width,canvasFront.height,ctxFront,true,yellow,fade);

        if (this.frontMenu === "Options") {
            ctxFront.font = "50px Arial";
            ctxFront.fillStyle = night;
            ctxFront.fillText("A.I.lchemy - August 2022", 80, 150);
            ctxFront.fillText("Author: Robin (robinpan.itch.io)", 80, 250);
            ctxFront.fillText("for the Summer Incremental Game Jam 2022", 80, 350);
        }

        if (this.frontMenu === "Help") {
            ctxFront.font = "20px Arial";
            ctxFront.fillStyle = night;
            ctxFront.lineCap = "round";
            ctxFront.lineWidth = 8;
            ctxFront.strokeStyle = purple;
            drawLine(ctxFront, 450, 40, 450, 80);
            ctxFront.fillText("People note the app", 360, 110);
            ctxFront.fillText("determines the popularity", 335, 135);
            ctxFront.fillText("and number of newcomers", 330, 160);
            drawLine(ctxFront, 670, 40, 720, 80);
            ctxFront.fillText("Get money for each match", 630, 110);
            ctxFront.fillText("(even the terrible ones)", 650, 135);
            drawLine(ctxFront, 150, 150, 190, 200);
            ctxFront.fillText("Research and develop your A.I.,", 55, 225);
            ctxFront.fillText("attract new customers,", 90, 250);
            ctxFront.fillText("and automate away the tedious parts", 30, 275);
            ctxFront.fillText("Research max depends on all time couples", 10, 300);
            drawLine(ctxFront, 800, 350, 800, 400);
            ctxFront.fillText("Let people complete their profile,", 653, 425);
            ctxFront.fillText("and try to match them together.", 660, 450);
            ctxFront.fillText("Bad matches create weak couples, eventually returning to the app.", 520, 475);
            ctxFront.fillText("Good matches create definitive couples, not coming back", 550, 500);
            ctxFront.fillText("Hover to speed up the profile processus, and see differents stats.", 520, 525);
            ctxFront.fillText("Hold click to select a person, and hover another to match them together.", 490, 550);
        }

        if (this.frontMenu === "win") {
            ctxFront.font = "50px Arial";
            ctxFront.fillStyle = night;
            ctxFront.fillText("Your app has been bought by a big corporation", 90, 150);
            ctxFront.fillText("You win !", 500, 250);
            ctxFront.fillText("You can continue playing if you want,", 200, 380);
            ctxFront.fillText("but there isn't anything left to do", 260, 440);
        }
    }

    draw() {
        ctxMenu.clearRect(0,0,canvasMenu.width,canvasMenu.height);

        this.drawMenu();

        this.buttons.tabs.forEach(button => {
            button.draw(mouse.x, mouse.y);
            if (button.active) {
                if (button.title === "TabResearch") {
                    upgradeHandler.draw();
                    const researchMax = upgradeHandler.getResearchMax();
                    this.buttons.TabResearch.forEach(button => {
                        const num = Number(button.text);
                        if (
                            !Number.isNaN(num) &&
                            Math.abs(num) <= researchMax
                        ) {
                            button.draw(mouse.x, mouse.y);
                        }
                    })
                }
                if (button.title === "TabAI") {
                    statHandler.draw();
                }
                if (button.title === "TabMarketing") {
                    marketingHandler.draw();
                }
            }
        });
        this.buttons.options.forEach(button => {
            button.draw(mouse.x, mouse.y);
        })
    }

    core() {

    }
}