class GameHandler {
    constructor() {
        this.amIWorking = false;
    }

    async initialize() {
        this.reinit();
        await this.loadImages();
    }

    reinit() {
        saveHandler.load();
        menuHandler.initialize();
        upgradeHandler.initialize();
        marketingHandler.initialize();
        cardHandler.initGrid();
    }
    
    async loadImages() {
        const promiseArray = [];
    
        for (let key of Object.keys(img)) {
            promiseArray.push(new Promise(resolve => {
                img[key] = new Image();
                img[key].onload = () => {
                    resolve();
                };
                img[key].src = "assets/img/"+key+".png";
            }));
        }

        await Promise.all(promiseArray);
    }

    mouseDown() {
        if (menuHandler.click()) {
            return;
        }
        if (cardHandler.mouseDown()) {
            return;
        }
    }

    mouseUp() {
        cardHandler.mouseUp();
    }
    
    draw() {
        cardHandler.draw();
        messageHandler.draw();
        menuHandler.draw();
        currencyHandler.draw();
        aiHandler.draw();
    }

    core() {
        marketingHandler.core();
        aiHandler.core();
        cardHandler.core();
        messageHandler.core();
        menuHandler.core();
        currencyHandler.core();
        upgradeHandler.core();
        statHandler.core();
        saveHandler.core();

        this.draw();
    }
}