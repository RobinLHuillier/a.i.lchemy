class CardHandler {
    constructor() {
        const maxStock = 2000;
        this.nbCellX = 0;
        this.nbCellY = 0;
        this.grid = [];
        this.firstPartOfMatch = [-1,-1];
        this.queueFading = [];
        this.queueAppear = [];

        this.stock = new Array(maxStock);
        for (let i=0; i<maxStock; i++) {
            this.stock[i] = new Card();
        }
        this.stockPosition = 0;
        this.occupiedPosition = [];
        this.freePosition = [];
        this.simplified = false;
        this.ultraSimplified = false;
    }

    initGrid() {
        this.occupiedPosition = [];
        this.freePosition = [];
        this.nbCellX = Math.round(save.grid.sizeX / save.grid.cell.sizeX);
        this.nbCellY = Math.round(save.grid.sizeY / save.grid.cell.sizeY);
        this.grid = new Array(this.nbCellX);
        for (let i=0; i<this.nbCellX; i++) {
            this.grid[i] = new Array(this.nbCellY);
            for (let j=0; j<this.nbCellY; j++) {
                this.freePosition.push(serializePosition(i,j));
            }
        }
        shuffle(this.freePosition);
        this.resetCanvas();
        this.checkSimplified();
    }

    reinitGrid() {
        const oldX = this.nbCellX;
        const oldY = this.nbCellY;
        const sizeX = save.grid.cell.sizeX;
        const sizeY = save.grid.cell.sizeY;
        this.nbCellX = Math.round(save.grid.sizeX / save.grid.cell.sizeX);
        this.nbCellY = Math.round(save.grid.sizeY / save.grid.cell.sizeY);
        this.grid.length = this.nbCellX;
        this.checkSimplified();
        for (let i=0; i<this.nbCellX; i++) {
            if (i<oldX) {
                this.grid[i].length = this.nbCellY;
            } else {
                this.grid[i] = new Array(this.nbCellY);
            }
        }
        for (let i=0; i<this.nbCellX; i++) {
            for (let j=0; j<this.nbCellY; j++) {
                if (i<oldX && j<oldY) {
                    if (undefined !== this.grid[i][j]) {
                        const realX = save.grid.offsetX + sizeX * i;
                        const realY = save.grid.offsetY + sizeY * j;
                        this.grid[i][j].changeSize(sizeX-6, sizeY-6, realX+3, realY+3, this.simplified, this.ultraSimplified);
                    }
                } else {
                    this.freePosition.push(serializePosition(i,j));
                }
            }
        }
        shuffle(this.freePosition);
        this.resetCanvas();
    }

    checkSimplified() {
        if (save.grid.cell.sizeX < 100) {
            this.simplified = true;
        } 
        if (save.grid.cell.sizeX < 50) {
            this.ultraSimplified = true;
        }
    }

    generateNewCards(quantity = 1) {
        const free = this.getFreePositions();
        if (free.length === 0) {
            return false;
        }
        quantity = statHandler.checkPotentialPool(quantity);
        if (quantity === 0) {
            if (!save.marketing.instant.unlocked) {
                // protection against softlock
                upgradeHandler.effectUpgrade("marketInstant");
            }
            return false;
        }

        for (let i=0; i<quantity && i<free.length; i++) {
            this.setNewCard(this.getNewCard(), ...unserializePosition(free[i]));
        }
        return true;
    }

    getNewCard() {
        let card = this.stock[this.stockPosition];
        this.stockPosition++;
        if (this.stockPosition >= this.stock.length) {
            this.stockPosition = 0;
        }
        return card;
    }

    setNewCard(card, x, y) {
        if (x < 0 || x >= this.nbCellX || y < 0 || y >= this.nbCellY) {
            console.error('[cardHandler.setNewCard] trying to access outside the grid')
            return;
        }

        const sizeX = save.grid.cell.sizeX;
        const sizeY = save.grid.cell.sizeY;
        const realX = save.grid.offsetX + sizeX * x;
        const realY = save.grid.offsetY + sizeY * y;
        card.reset(realX+3, realY+3, sizeX-6, sizeY-6, this.simplified, this.ultraSimplified, save.cards.infoQuantity);
        this.grid[x][y] = card;
        delElem(this.freePosition, serializePosition(x,y));
        this.queueAppear.push({
            'x': x,
            'y': y,
            'card': card,
        });
    }

    getOccupiedPositions() {
        return this.occupiedPosition;
    }

    getFreePositions() {
        return this.freePosition;
    }

    setFreePosition(x, y) {
        if (x < 0 || x >= this.nbCellX || y < 0 || y >= this.nbCellY || this.grid[x][y] === undefined) {
            console.error('[cardHandler.setFreePosition] trying to access outside the grid or the cell is undefined', x, y);
            return;
        }
        delElem(this.occupiedPosition, serializePosition(x, y));
        this.queueFading.push({
            'x': x,
            'y': y,
            'card': this.grid[x][y]
        });
    }

    advanceFadeQueue() {
        this.queueFading.forEach(elem => {
            if (elem.card.fadeToBlack()) {
                this.grid[elem.x][elem.y] = undefined;
                elem.card.resetDraw();
                this.freePosition.push(serializePosition(elem.x, elem.y));
                elem.x = -1;
            }
        });
        this.queueFading = this.queueFading.filter(elem => elem.x > -1);

        this.queueAppear = this.queueAppear.filter(elem => {
            if (elem.card.fadeToWhite()) {
                this.occupiedPosition.push(serializePosition(elem.x, elem.y));
                return false;
            }
            return true;
        });
    }

    advanceDiscovery() {
        if (this.simplified) {
            return;
        }
        const highlight = this.getMouseHoverPosition();
        for (let i=0; i<this.nbCellX; i++) {
            for (let j=0; j<this.nbCellY; j++) {
                if (undefined !== this.grid[i][j]) {
                    let mult = 1;
                    if (i === highlight[0] && j === highlight[1]) {
                        mult = save.cards.discoveryMultiplier;
                    }
                    if (this.grid[i][j].advanceDiscovery(mult)) {
                        const realX = save.grid.offsetX + i * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
                        const realY = save.grid.offsetY + j * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
                        messageHandler.addMessage(realX, realY, "Deleting the app", pink, 1);
                        messageHandler.addMessageStars(0);
                        currencyHandler.addStars(0);
                        this.setFreePosition(i, j);
                    }
                }
            }
        }
    }

    checkMatch() {
        const secondPartOfMatch = this.getMouseHoverPosition();

        // match not on cards
        if (
            secondPartOfMatch[0] === -1 ||
            this.firstPartOfMatch[0] === -1 ||
            this.grid[secondPartOfMatch[0]][secondPartOfMatch[1]] === undefined ||
            (secondPartOfMatch[0] === this.firstPartOfMatch[0] && secondPartOfMatch[1] === this.firstPartOfMatch[1]) ||
            1 > this.grid[secondPartOfMatch[0]][secondPartOfMatch[1]].fade
        ) {
            this.firstPartOfMatch = [-1,-1];
            return false;
        }

        // profile incomplete
        if (
            !this.grid[this.firstPartOfMatch[0]][this.firstPartOfMatch[1]].isAllDiscovered() ||
            !this.grid[secondPartOfMatch[0]][secondPartOfMatch[1]].isAllDiscovered()
        ) {
            const realX = save.grid.offsetX + secondPartOfMatch[0] * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
            const realY = save.grid.offsetY + secondPartOfMatch[1] * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
            messageHandler.addMessage(realX, realY, "Profile incomplete!", blue);
            this.firstPartOfMatch = [-1,-1];
            return false;
        }
        
        const notation = this.matchNotation(...this.firstPartOfMatch, ...secondPartOfMatch);
        this.completeMatch(notation, ...this.firstPartOfMatch, ...secondPartOfMatch, blue);

        this.firstPartOfMatch = [-1,-1];
        return true;
    }

    completeMatch(notation, x1, y1, x2, y2, color) {
        const realX = save.grid.offsetX + x2 * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
        const realY = save.grid.offsetY + y2 * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
        messageHandler.addMessage(realX, realY, notation[0], color, notation[1]);

        currencyHandler.addMoney(save.currency.matchEarnings, true);
        currencyHandler.addStars(notation[2]);

        messageHandler.addMessageStars(notation[2]);

        statHandler.sortCustomerCouples(notation[2]);

        this.setFreePosition(x1, y1);
        this.setFreePosition(x2, y2);
    }

    matchNotation(x1, y1, x2, y2) {
        const card1 = this.grid[x1][y1];
        const card2 = this.grid[x2][y2];
        if (card1 === undefined || card2 === undefined) {
            return "";
        }

        let points = 0;

        points += compareGenderAndSexuality(card1, card2) ? 5 : -100; 
        points += compareAge(card1, card2) ? 2 : -5;
        points += compareTypeRelation(card1, card2) ? 2 : -1;
        points -= 3*compareLikesInDislikes(card1, card2);
        points += 2*compareLikesInLikesAndDislikesInDislikes(card1, card2);

        return pointsToNotation(points);
    }

    mouseDown() {
        if (mouse.x < save.grid.offsetX || mouse.y < save.grid.offsetY) {
            return false;
        }
        const highlight = this.getMouseHoverPosition();
        if (highlight[0] !== -1 && undefined !== this.grid[highlight[0]][highlight[1]]) {
            this.firstPartOfMatch = highlight;
        }
        return true;
    }

    mouseUp() {
        if (
            mouse.x < save.grid.offsetX ||
            mouse.x > save.grid.offsetX + save.grid.sizeX ||
            mouse.y < save.grid.offsetY ||
            mouse.y > save.grid.offsetY + save.grid.sizeY ||
            -1 === this.firstPartOfMatch[0]
        ) {
            return false;
        }
        if (undefined === this.grid[this.firstPartOfMatch[0]][this.firstPartOfMatch[1]] || 1 > this.grid[this.firstPartOfMatch[0]][this.firstPartOfMatch[1]].fade) {
            this.firstPartOfMatch = [-1,-1];
            return;
        }
        return this.checkMatch();
    }

    getMouseHoverPosition() {
        if (mouse.x === undefined || mouse.y === undefined) {
            return [-1,-1];
        }
        const x = Math.floor((mouse.x - save.grid.offsetX)/save.grid.cell.sizeX);
        const y = Math.floor((mouse.y - save.grid.offsetY)/save.grid.cell.sizeY);
        if (x < 0 || y < 0 || x >= this.nbCellX || y >= this.nbCellY) {
            return [-1, -1];
        }
        return [x, y];
    }
    
    draw() {
        const highlight = this.getMouseHoverPosition();

        this.drawCards(highlight);
        this.drawMatchConnection();
        if (save.ai.hoverUnlocked) {
            this.drawMatchHover(highlight);
        }
    }

    resetCanvas() {
        ctxCards.clearRect(0,0,canvasCards.width,canvasCards.height);
    }

    drawCards(highlight) {
        for (let i=0; i<this.nbCellX; i++) {
            for (let j=0; j<this.nbCellY; j++) {
                if (undefined !== this.grid[i][j]) {
                    if (
                        (i === highlight[0] && j === highlight[1]) ||
                        (i === this.firstPartOfMatch[0] && j === this.firstPartOfMatch[1])
                    ) {
                        this.grid[i][j].draw(true);
                    } else {
                        this.grid[i][j].draw(false);
                    }
                    if (i === highlight[0] && j === highlight[1] && save.cards.infoQuantity > 3) {
                        this.grid[i][j].displaySecondaryInfos();
                    } else {
                        this.grid[i][j].displayPrimaryInfos();
                    }
                }
            }
        }
    }

    drawMatchConnection() {
        ctxMatch.clearRect(0,0,canvasMatch.width, canvasMatch.height);
        if (this.firstPartOfMatch[0] !== -1 && this.firstPartOfMatch[1] !== -1) {
            const realX = save.grid.offsetX + this.firstPartOfMatch[0] * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
            const realY = save.grid.offsetY + this.firstPartOfMatch[1] * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
            ctxMatch.lineWidth = 10;
            ctxMatch.lineCap = "round";
            ctxMatch.strokeStyle = blue;
            ctxMatch.beginPath();
            ctxMatch.moveTo(realX, realY);
            let capX = mouse.x;
            let capY = mouse.y
            if (capX > save.grid.offsetX + save.grid.sizeX) {
                capX = save.grid.offsetX + save.grid.sizeX;
            } else if (capX < save.grid.offsetX) {
                capX = save.grid.offsetX;
            }
            if (capY > save.grid.offsetY + save.grid.sizeY) {
                capY = save.grid.offsetY + save.grid.sizeY;
            } else if (capY < save.grid.offsetY) {
                capY = save.grid.offsetY;
            }
            ctxMatch.lineTo(capX, capY);
            ctxMatch.stroke();
        }
    }

    drawMatchHover(highlight) {
        if (
            (highlight[0] !== this.firstPartOfMatch[0] || highlight[1] !== this.firstPartOfMatch[1]) &&
            (highlight[0] !== -1 && this.firstPartOfMatch[0] !== -1) 
        ) {
            let notation;
            if (
                !this.grid[highlight[0]][highlight[1]]?.isAllDiscovered() ||
                !this.grid[this.firstPartOfMatch[0]][this.firstPartOfMatch[1]]?.isAllDiscovered()
            ) {
                notation = ["Profile incomplete"];
            } else {
                notation = this.matchNotation(...this.firstPartOfMatch, ...highlight, );
            }
            if (notation[0] !== undefined) {
                const offsetX = notation[0].length*5;
                const offsetY = 30;
                const lineSize = 25;

                let diffX = 0;
                let diffY = 0;
                const off = 14;
                if (mouse.x - offsetX < save.grid.offsetX + off) {
                    diffX = save.grid.offsetX + off - (mouse.x - offsetX);
                } else if (mouse.x + offsetX > save.grid.offsetX + save.grid.sizeX - off) {
                    diffX = save.grid.offsetX + save.grid.sizeX - off - (mouse.x + offsetX);
                }
                if (mouse.y - offsetY < save.grid.offsetY + off) {
                    diffY = save.grid.offsetY + off - (mouse.y - offsetY);
                }

                const x1 = mouse.x - offsetX + diffX;
                const x2 = mouse.x + offsetX + diffX;
                const y1 = mouse.y - offsetY + diffY;
                const y2 = mouse.y - offsetY + diffY;

                ctxMatch.lineWidth = lineSize + 5;
                ctxMatch.strokeStyle = pink;
                ctxMatch.lineCap = "round";
                ctxMatch.beginPath();
                ctxMatch.moveTo(x1, y1);
                ctxMatch.lineTo(x2, y2);
                ctxMatch.stroke();

                ctxMatch.font = lineSize.toString() + "px Arial";
                ctxMatch.fillStyle = yellow;
                ctxMatch.fillText(notation[0], x1+2, y1+7, 2*offsetX-4);
            }
        }
    }

    core() {        
        this.advanceDiscovery();
        this.advanceFadeQueue();
    }
}