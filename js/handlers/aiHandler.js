class AiHandler {
    constructor() {
        this.advancement = 0;
        this.stickQueue = [];
    }

    advance() {
        if (!save.ai.unlocked) {
            return;
        }
        this.advancement += save.ai.speed;
        if (this.advancement >= 100) {
            if (this.autoMatch()) {
                this.advancement -= 100;
            } else {
                this.advancement = 100;
            }
        }

        this.stickQueue = this.stickQueue.filter(stick => {
            stick.fade -= 0.1;
            return stick.fade > 0;
        });
    }

    addStick(x1, y1, x2 ,y2) {
        const realX1 = save.grid.offsetX + x1 * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
        const realY1 = save.grid.offsetY + y1 * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
        const realX2 = save.grid.offsetX + x2 * save.grid.cell.sizeX + save.grid.cell.sizeX / 2;
        const realY2 = save.grid.offsetY + y2 * save.grid.cell.sizeY + save.grid.cell.sizeY / 2;
        this.stickQueue.push({
            'x1': realX1,
            'y1': realY1,
            'x2': realX2,
            'y2': realY2,
            'fade': 1,
        });
    }

    autoMatch() {
        const free = cardHandler.getOccupiedPositions();
        if (free.length < 2) {
            return false;
        }
        for (let i=0; i<save.ai.quantity && i<Math.floor(free.length/2); i++) {
            const points = rand(save.ai.notationMax+1, save.ai.notationMin);
            const notation = pointsToNotation(points);
            const pos1 = unserializePosition(free[2*i]);
            const pos2 = unserializePosition(free[2*i +1]);
            if (pos1 === undefined || pos2 === undefined) {
                return true;
            }
            this.addStick(...pos1, ...pos2);
            cardHandler.completeMatch(notation, ...pos1, ...pos2, pink);
        }
        return true;
    }

    draw() {
        if (!save.ai.unlocked) {
            return;
        }

        ctxBanner.lineWidth = 30;
        ctxBanner.strokeStyle = gold;
        const x = save.grid.offsetX + save.grid.sizeX - 45;
        const y = save.grid.offsetY - 15;
        const h = save.grid.offsetY - 24;
        ctxBanner.beginPath();
        ctxBanner.moveTo(x,y);
        ctxBanner.lineTo(x,y-0.01*this.advancement*h);
        ctxBanner.stroke();

        this.stickQueue.forEach(stick => {
            ctxMatch.lineWidth = 10;
            ctxMatch.lineCap = "round";
            ctxMatch.strokeStyle = addFade(pink, stick.fade);
            ctxMatch.beginPath();
            ctxMatch.moveTo(stick.x1, stick.y1);
            ctxMatch.lineTo(stick.x2, stick.y2);
            ctxMatch.stroke();
        });
    }

    core() {
        this.advance();
    }
}