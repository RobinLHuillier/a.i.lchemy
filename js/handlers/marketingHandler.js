class MarketingHandler {
    constructor() {
        this.buttons = [];
    }

    initialize() {
        const width = save.grid.offsetX-50;
        const height = 170;
        const offsetX = 20;
        const offsetY = save.grid.offsetY+60;
        this.buttons.push(new MarketingButton(offsetX, offsetY, width, height, ctxMenu, 2, pink, white, night, 20, "Word of Mouth",undefined, "TabMarketing", yellow, blue, purple, yellow, save.marketing.instant.unlocked, true, "instant", save.marketing.instant.cost, save.marketing.instant.speed, save.marketing.instant.flavorText, save.marketing.instant.quantity));
        this.buttons.push(new MarketingButton(offsetX, offsetY+height+10, width, height, ctxMenu, 2, pink, white, night, 20, "Guerilla Marketing",undefined, "TabMarketing", yellow, blue, purple, yellow, save.marketing.progress.unlocked, true, "progress", save.marketing.progress.cost, save.marketing.progress.speed, save.marketing.progress.flavorText, save.marketing.progress.quantity));
        this.buttons.push(new MarketingButton(offsetX, offsetY+2*height+20, width, height, ctxMenu, 2, pink, white, night, 20, "Growth Marketing",undefined, "TabMarketing", yellow, blue, purple, yellow, save.marketing.perma.unlocked, true, "perma", save.marketing.perma.cost, save.marketing.perma.speed, save.marketing.perma.flavorText, save.marketing.perma.quantity, save.marketing.perma.active));
    }

    save() {
        this.buttons.forEach(button => {
            save.marketing[button.type].cost = button.cost;
            save.marketing[button.type].speed = button.speed;
            save.marketing[button.type].flavorText = button.flavorText;
            save.marketing[button.type].quantity = button.quantity;
            save.marketing[button.type].active = button.active;
        });
    }

    getButton(type) {
        let button;
        for (let i=0; i<this.buttons.length; i++) {
            if (this.buttons[i].type === type) {
                button = this.buttons[i];
            }
        }
        if (undefined === button) {
            console.error("[MarketingHandler.getButton] type undefined");
        }
        return button;
    }

    unlock(type) {
        this.getButton(type).visible = true;
    }

    updateMarketing() {
        this.buttons.forEach(button => {
            button.cost = save.marketing[button.type].cost;
            button.speed = save.marketing[button.type].speed;
            button.flavorText = save.marketing[button.type].flavorText;
            button.quantity = save.marketing[button.type].quantity;
            button.active = save.marketing[button.type].active;
        });
    }

    changeQuantitySpeedCost(type, quantity, speed, cost) {
        const button = this.getButton(type);
        button.quantity = quantity;
        button.speed = speed;
        button.cost = cost;
    }

    click() {
        let clicked = false;
        this.buttons.forEach(button => {
            const result = button.click(mouse.x, mouse.y);
            if (result) {
                clicked = true;
            }
        });
        return clicked;
    }

    draw() {
        this.buttons.forEach(button => button.draw(mouse.x, mouse.y));
    }

    core() {
        this.buttons.forEach(button => button.advance());
    }
}