class MessageHandler {
    constructor() {
        const maxStock = 50;

        this.queue = [];
        this.stock = new Array(maxStock);
        for (let i=0; i<maxStock; i++) {
            this.stock[i] = new Message();
        }
        this.stockPosition = 0;
    }

    getNewMessage() {
        let message = this.stock[this.stockPosition];
        this.stockPosition++;
        if (this.stockPosition >= this.stock.length) {
            this.stockPosition = 0;
        }
        return message;
    }

    addMessage(x,y,text,color, textSizeMult = 1) {
        let message = this.getNewMessage();
        message.reset(x,y,text,color, textSizeMult);
        this.queue.push(message);
    }

    addMessageStars(quantity) {
        this.addMessage(save.grid.offsetX+150, 75, quantity+" Star"+(quantity>1?"s":""), gold, 1);
    }

    addMessageMoney(quantity) {
        this.addMessage(save.grid.offsetX+300, 75, quantity.toString()+"$", gold, 1);
    }

    advance() {
        this.queue = this.queue.filter(msg => !msg.advance());
    }

    draw() {
        ctxMessage.clearRect(0,0,canvasMessage.width,canvasMessage.height);
        this.queue.forEach(msg => msg.draw());
    }

    core() {
        this.advance();
    }
}