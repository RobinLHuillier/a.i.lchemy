class SaveHandler {
    constructor() {
        this.saveName = "ccc"; // TODO
        this.countToSave = 4;//Math.round(1000/refresh)*10; // every 10 sec
        this.count = 0;
    }

    load() {
        const file = JSON.parse(localStorage.getItem(this.saveName));
        if (null === file || undefined === file || 0 === file.length) {
            resetSave();
        } else {
            save = file;
        }
    }

    save() {
        upgradeHandler.save();
        marketingHandler.save();
        localStorage.setItem(this.saveName, JSON.stringify(save));
    }

    resetSave() {
        localStorage.removeItem(this.saveName);
    }

    advanceCount() {
        this.count--;
        if (this.count <= 0) {
            this.count = this.countToSave;
            return true;
        }
        return false;
    }

    core() {
        if(this.advanceCount()) {
            this.save();
        }
    }
}