class UpgradeHandler {
    constructor() {
        this.upgrades = [];
        this.moneyPerTick = 0;
        this.moneyPerSec = 0;
        this.showMax = 5;
        this.numberOfActiveUpgrades = 0;
    }

    initialize() {
        this.upgrades = [];
        this.numberOfActiveUpgrades = 0;
        save.upgrades.forEach(upgrade => {
            if (!upgrade.bought) {
                this.upgrades.push(new Upgrade(...upgrade));
                if (upgrade.moneyPerTick > 0) {
                    this.numberOfActiveUpgrades++;
                }
            }
        });
        this.sortUpgrades();
    }

    save() {
        save.upgrades = [];
        this.upgrades.forEach(upgrade => {
            save.upgrades.push(upgrade.serialize());
        });
    }

    sortUpgrades() {
        this.upgrades.sort((up1, up2) => up1.cost - up2.cost);
    }

    changeResearchAmount(serializedAmount) {
        if (serializedAmount === "Research0") {
            this.moneyPerSec = 0;
        }
        
        let amount = 0;
        switch (serializedAmount) {
            case "Research-100":
                amount -= 100;
                break;
            case "Research-10":
                amount = -10;
                break;
            case "Research-1":
                amount = -1;
                break;
            case "Research+1":
                amount = +1;
                break;
            case "Research+10":
                amount = +10;
                break;
            case "Research+100":
                amount += 100;
                break;
            default:
                break;
        }
        this.moneyPerSec += amount;
        if (this.moneyPerSec < 0) {
            this.moneyPerSec = 0;
        }
        if (this.moneyPerSec > this.getResearchMax()) {
            this.moneyPerSec = this.getResearchMax();
        }
        this.updateMoneyPerTick();
    }

    getResearchMax() {
        return 1+save.research.bonusMax+(save.people.allTimeCouple/save.research.ratio);
    }

    advanceUpgrades() {
        if (this.numberOfActiveUpgrades === 0) {
            return;
        }

        let money = currencyHandler.howMuchMoney();
        let toDel = [];
        for (let i=0; i<this.showMax && i<this.upgrades.length; i++) {
            const upgrade = this.upgrades[i];
            let quantity = upgrade.moneyPerTick;
            if (quantity > money) {
                quantity = money;
            }
            const rem = upgrade.advance(quantity);
            money -= quantity;
            if (rem > 0) {
                money += rem;
                toDel.push(upgrade);
            }
        }
        toDel.forEach(upgrade => {
            this.finishUpgrade(upgrade);
        });

        currencyHandler.payMoney(currencyHandler.howMuchMoney() - money);
        if (toDel.length > 0) {
            this.sortUpgrades();
            this.updateMoneyPerTick();
        }
    }

    finishUpgrade(upgrade) {
        upgrade.bought = true;
        delElem(this.upgrades, upgrade);
        this.numberOfActiveUpgrades--;
        this.effectUpgrade(upgrade.effect);
    }

    effectUpgrade(effect) {
        switch(effect) {
            case "moneyMatch1":
                save.currency.matchEarnings += 1;
                break;
            case "moneyMatch2":
                save.currency.matchEarnings += 2;
                break;
            case "moneyMatch3":
                save.currency.matchEarnings += 7;
                break;
            case "moneyMatch4":
                save.currency.matchEarnings += 10;
                break;
            case "moneyMatch5":
                save.currency.matchEarnings += 25;
                break;
            case "moneyMatch6":
                save.currency.matchEarnings += 50;
                break;
            case "moneyMatch7":
                save.currency.matchEarnings += 100;
                break;
            case "moneyMatch8":
                save.currency.matchEarnings += 400;
                break;
            case "discoverySpeed":
                save.cards.discoverySpeed *= 2;
                break;
            case "instantDiscovery":
                save.cards.instantDiscovery = true;
                break;
            case "degen":
                save.cards.degenSpeed /= 2;
                break;
            case "noDegen":
                save.cards.degenSpeed = 0;
                break;
            case "matchHover":
                save.ai.hoverUnlocked = true;
                break;
            case "noteReset":
                save.currency.numberOfMatches = 0
                save.currency.stars = 0;
                this.lockUpgrade("sellData");
                break;
            case "sellData":
                currencyHandler.addMoney(25*save.currency.numberOfMatches);
                this.lockUpgrade("noteReset");
                break;
            case "matchAuto":
                save.ai.unlocked = true;
                break;
            case "matchSpeed":
                save.ai.speed *= 2;
                break;
            case "matchQuality1":
                save.ai.notationMin = -5;
                save.ai.notationMax = 6;
                break;
            case "matchQuality2":
                save.ai.notationMin = 0;
                save.ai.notationMax = 8;
                break;
            case "matchQuality3":
                save.ai.notationMin = 2;
                save.ai.notationMax = 11;
                break;
            case "matchQuality4":
                save.ai.notationMin = 5;
                save.ai.notationMax = 13;
                break;
            case "boardSize1":
                save.grid.cell.sizeX = 200;
                save.grid.cell.sizeY = 200;
                cardHandler.reinitGrid();
                break;
            case "boardSize2":
                save.grid.cell.sizeX = 100;
                save.grid.cell.sizeY = 100;
                cardHandler.reinitGrid();
                break;
            case "boardSize3":
                save.grid.cell.sizeX = 50;
                save.grid.cell.sizeY = 50;
                cardHandler.reinitGrid();
                break;
            case "boardSize4":
                save.grid.cell.sizeX = 20;
                save.grid.cell.sizeY = 20;
                cardHandler.reinitGrid();
                break;
            case "marketInstant":
                save.marketing.unlocked = true;
                menuHandler.unlock("TabMarketing");
                save.marketing.instant.unlocked = true;
                marketingHandler.unlock("instant");
                marketingHandler.updateMarketing();
                break;
            case "marketProgress":
                save.marketing.progress.unlocked = true;
                marketingHandler.unlock("progress");
                marketingHandler.updateMarketing();
                break;
            case "marketPerma":
                save.marketing.perma.unlocked = true;
                marketingHandler.unlock("perma");
                marketingHandler.updateMarketing();
                break;
            case "marketInstantx2":
                save.marketing.instant.quantity *= 2;
                marketingHandler.updateMarketing();
                break;
            case "marketInstantx5":
                save.marketing.instant.quantity *= 5;
                marketingHandler.updateMarketing();
                break;
            case "marketPerma2":
                save.marketing.perma.quantity += 4;
                marketingHandler.updateMarketing();
                break;
            case "marketPerma3":
                save.marketing.perma.quantity += 15;
                marketingHandler.updateMarketing();
                break;
            case "marketPerma4":
                save.marketing.perma.quantity += 80;
                marketingHandler.updateMarketing();
                break;    
            case "info+":
                save.cards.infoQuantity++;
                break;
            case "starMultx2":
                save.currency.starMultiplier *= 2;
                break;
            case "marketProgressx2":
                save.marketing.progress.cost *= 2;
                save.marketing.progress.speed /= 5;
                marketingHandler.updateMarketing();
                break;
            case "labMax1":
                save.research.bonusMax += 10;
                break;
            case "labMax2":
                save.research.bonusMax += 30;
                break;
            case "labMax3":
                save.research.ratio--;
                break;
            case "maxInterns":
                save.research.bonusMax += 100;
                this.lockUpgrade("accelerateResearch");
                break;
            case "accelerateResearch":
                save.research.ratio -= 5;
                this.lockUpgrade("maxInterns");
                break;
            case "matchQuantity":
                save.ai.quantity +=1;
                break;
            case "win":
                menuHandler.win();
                break;
            default:
                console.error("[upgradeHandler.effectUpgrade] this effect doesn't exist");
                break;
        }
    }

    lockUpgrade(effect) {
        for (let i=0; i<this.upgrades.length; i++) {
            const up = this.upgrades[i];
            if (up.effect === effect) {
                delElem(this.upgrades, up);
                return;
            }
        }
    }

    numberOfResearchActive() {
        let count = 0;
        for (let i=0; i<this.showMax && i<this.upgrades.length; i++) {
            if (this.upgrades[i].moneyPerTick > 0) {
                count++;
            }
        }
        return count;
    }

    updateMoneyPerTick() {
        this.moneyPerTick = this.moneyPerSec / tickPerSec;
        let share = this.moneyPerTick/this.numberOfResearchActive();
        if (share === 0) {
            share = 0.0000001;
        }
        for (let i=0; i<this.showMax && i<this.upgrades.length; i++) {
            if (this.upgrades[i].moneyPerTick > 0) {
                this.upgrades[i].moneyPerTick = share;
            }
        }
    }

    toggleActive(upgrade) {
        if (upgrade.moneyPerTick > 0) {
            this.numberOfActiveUpgrades--;
            upgrade.moneyPerTick = 0;
        } else {
            this.numberOfActiveUpgrades++;
            upgrade.moneyPerTick = 0.0001;
        }
        this.updateMoneyPerTick();
    }

    click() {
        const offsetX = 40;
        const offsetY = save.grid.offsetY + 150;
        const heightUp = 90;
        const widthUp = 300;

        for (let i=0; i<this.showMax && i<this.upgrades.length; i++) {
            let hovered = true;
            if (mouse.x < offsetX || mouse.x > offsetX+widthUp || mouse.y < offsetY+i*heightUp || mouse.y > offsetY+(i+1)*heightUp) {
                hovered = false;
            }
            if (hovered) {
                this.toggleActive(this.upgrades[i]);
                return true;
            }
        }
        return false;
    }

    formatMoneySpent() {
        return Math.floor(this.moneyPerSec).toString();
    }

    draw() {
        this.drawResearch()
        this.drawUpgrades();
    }

    drawResearch() {
        const offsetX = 40;
        const offsetY = save.grid.offsetY + 75;
        const width = 300;

        ctxMenu.font = "20px Arial";
        ctxMenu.fillStyle = blue;
        ctxMenu.fillText("Money spent per sec: " + this.formatMoneySpent() + "/" + formatNumber(this.getResearchMax()), offsetX, offsetY, width);
    }

    drawUpgrades() {
        const offsetX = 40;
        const offsetY = save.grid.offsetY + 150;
        const heightUp = 90;
        const widthUp = 300;

        for (let i=0; i<this.showMax && i<this.upgrades.length; i++) {
            let hovered = true;
            if (mouse.x < offsetX || mouse.x > offsetX+widthUp || mouse.y < offsetY+i*heightUp || mouse.y > offsetY+(i+1)*heightUp) {
                hovered = false;
            }
            this.upgrades[i].draw(offsetX, offsetY+i*heightUp, widthUp, heightUp-10, hovered);
        }
    }

    core() {
        this.advanceUpgrades();
    }
}