class CurrencyHandler {
    constructor() {
        this.moneyGoal = save.currency.money;
        this.inscription = 0;
    }

    addMoney(quantity, match=false) {
        save.currency.money += quantity;
        save.currency.allTimeEarnings += quantity;
        if (match) {
            messageHandler.addMessageMoney(quantity);
        }
    }

    payMoney(quantity) {
        if (save.currency.money < quantity) {
            return false;
        }
        save.currency.money -= quantity;
        return true;
    }

    howMuchMoney() {
        return save.currency.money;
    }
    
    addStars(quantity) {
        const count = save.currency.numberOfMatches;
        const oldAvg = save.currency.stars*count/(count+1);
        save.currency.stars = oldAvg + quantity/(count+1);
        save.currency.numberOfMatches++;
    }

    advanceMoneyGoal() {
        if (this.moneyGoal === save.currency.money) {
            return;
        }
        let diff = Math.abs(this.moneyGoal - save.currency.money)/5 + 1;
        if (this.moneyGoal > save.currency.money) {
            diff = -diff;
        }
        this.moneyGoal += diff;

        if (Math.abs(diff) < 2 || Math.round(this.moneyGoal) === Math.round(save.currency.money) || this.moneyGoal < 0) {
            this.moneyGoal = save.currency.money;
        }
    }

    getMultiplierInscription() {
        return (save.currency.stars < 1 ? 1 : save.currency.stars)*save.currency.starMultiplier;
    }

    advanceInscription() {
        this.inscription += this.getMultiplierInscription();
        if (this.inscription >= 100) {
            if (!cardHandler.generateNewCards()) {
                this.inscription = 100;
            } else {
                this.inscription -= 100;
            }
        }
    }

    formatStars() {
        return Math.floor(save.currency.stars * 100)/100;
    }

    drawMoney() {
        const offset = 250;
        ctxBanner.font = "25px Arial";
        ctxBanner.fillStyle = night;
        ctxBanner.fillText("$$$", save.grid.offsetX+offset, 35, 60);
        ctxBanner.font = "25px Arial";
        const money = formatNumber(this.moneyGoal);
        if (this.moneyGoal < save.currency.money) {
            ctxBanner.fillStyle = orange;
        } else if (this.moneyGoal > save.currency.money) {
            ctxBanner.fillStyle = pink;
        }
        ctxBanner.fillText(money, save.grid.offsetX+offset-2, 70, 100);
    }

    drawStars() {
        const offset = 5;
        const starWidth = 161;
        const starHeight = 30;
        ctxBanner.fillStyle = gold;
        ctxBanner.fillRect(save.grid.offsetX+offset,12,starWidth*(save.currency.stars/5),starHeight);

        ctxBanner.drawImage(img.stars, save.grid.offsetX+offset, 12);

        ctxBanner.font = "25px Arial";
        ctxBanner.fillStyle = night;
        ctxBanner.fillText("Stars: " + this.formatStars(), save.grid.offsetX+offset, 70, starWidth);
    }

    drawNewInscription() {
        ctxBanner.lineWidth = 30;
        ctxBanner.strokeStyle = blue;
        const x = save.grid.offsetX + save.grid.sizeX - 14;
        const y = save.grid.offsetY - 15;
        const h = save.grid.offsetY - 22;
        ctxBanner.beginPath();
        ctxBanner.moveTo(x,y);
        ctxBanner.lineTo(x,y-0.01*this.inscription*h);
        ctxBanner.stroke();
    }

    drawPool() {
        const offset = 360;
        ctxBanner.font = "25px Arial";
        ctxBanner.fillStyle = night;
        ctxBanner.fillText("Pool", save.grid.offsetX+offset, 35, 60);
        ctxBanner.font = "25px Arial";
        const pool = formatNumber(save.people.potentialPool);
        ctxBanner.fillText(pool, save.grid.offsetX+offset, 70, 100);
    }

    draw() {
        ctxBanner.clearRect(0,0,canvasBanner.width,canvasBanner.height);

        this.drawStars();
        this.drawMoney();
        this.drawPool();
        this.drawNewInscription();
    }

    core() {
        this.advanceMoneyGoal();
        this.advanceInscription();
    }
}