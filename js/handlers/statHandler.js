class StatHandler {
    constructor() {

    }

    customersComeBack() {
        if (save.people.weakCouples > 1) {
            const chanceToComeBack = 0.04;
            if (Math.random() < chanceToComeBack) {
                const maxToComeBack = save.people.weakCouples <= 100 ? 2 : Math.floor(save.people.weakCouples/50);
                const comingBack = 2*rand(Math.floor(maxToComeBack/2), 1);
                save.people.potentialPool += comingBack;
                save.people.weakCouples -= comingBack;
                if (save.people.weakCouples < 0) {
                    save.people.weakCouples = 0;
                    console.error("[StatHandler.customersComeBack] weak couples went under zero");
                }
            }
        }
    }

    checkPotentialPool(quantity) {
        let give = quantity;
        if (save.people.potentialPool < quantity) {
            give = save.people.potentialPool;
        }
        save.people.potentialPool -= give;
        return give;
    }

    sortCustomerCouples(notation) {
        save.people.allTimeCouple += 1;
        if (notation === 0) {
            return;
        }
        if (notation < 4) {
            save.people.weakCouples += 2;
            return;
        }
        save.people.definitiveCouples += 2;
    }

    draw() {
        const ctx = ctxMenu;
        ctx.fillStyle = night;
        ctx.font = "20px Arial";
        const text = [
            "Potential customers: " + formatNumber(save.people.potentialPool),
            "Weak couples: " + formatNumber(save.people.weakCouples),
            "Couples for life: " + formatNumber(save.people.definitiveCouples),
            "All time couples: " + formatNumber(save.people.allTimeCouple),
            "Number of notes: " + formatNumber(save.currency.numberOfMatches),
            formatNumber(save.currency.matchEarnings) + " $/match",
            "Profile info: " + (save.cards.instantDiscovery ? infSymbol : save.cards.discoverySpeed + " ticks (" + ticksToTime(1/save.cards.discoverySpeed) + ")"),
            "New user cooldown: " + ticksToTime(100/currencyHandler.getMultiplierInscription()),
            "Max research every " + formatNumber(save.research.ratio) + " couples",
            "Uninstall timer: " + (save.cards.degenSpeed === 0 ? infSymbol : ticksToTime(1/save.cards.degenSpeed)),
            save.ai.unlocked ? "Auto match speed: " + ticksToTime(100/save.ai.speed) : "",
            save.ai.unlocked ? "Note A.I.: " + pointsToNotation(save.ai.notationMin)[3] + " to " + pointsToNotation(save.ai.notationMax)[3] : "",
        ];
        for (let i=0; i<text.length; i++) {
            ctx.fillText(text[i], 25, 180+25*i, save.grid.offsetX-60);
        }
    }

    core() {
        this.customersComeBack();
    }
}