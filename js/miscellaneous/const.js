//canvas layers
const canvasBackground = document.getElementById('canvasBackground');
const ctxBackground = canvasBackground.getContext('2d');

const canvasCards = document.getElementById('canvasCards');
const ctxCards = canvasCards.getContext('2d');
const canvasMatch = document.getElementById('canvasMatch');
const ctxMatch = canvasMatch.getContext('2d');
const canvasMessage = document.getElementById('canvasMessage');
const ctxMessage = canvasMessage.getContext('2d');

const canvasMenuStatic = document.getElementById('canvasMenuStatic');
const ctxMenuStatic = canvasMenuStatic.getContext('2d');
const canvasBanner = document.getElementById('canvasBanner');
const ctxBanner = canvasBanner.getContext('2d');
const canvasMenu = document.getElementById('canvasMenu');
const ctxMenu = canvasMenu.getContext('2d');

const canvasFront = document.getElementById('canvasFront');
const ctxFront = canvasFront.getContext('2d');

//mouse
var canvasPosition = canvasFront.getBoundingClientRect();
var mouse = {
    x: undefined,
    y: undefined,
};

//time
const refresh = 50;
const tickPerSec = 1000/refresh;
const infSymbol = "∞";
var lastTime = 0;
var timer;

//img
const img = {
    "stars": null,
};
const sound = {};

//colors
const night = "rgb(13,43,69)";
const blue = "rgb(32,60,86)";
const purple = "rgb(84,78,104)";
const pink = "rgb(141,105,122)";
const orange = "rgb(208,129,89)";
const gold = "rgb(255,170,94)";
const yellow = "rgb(255,212,163)";
const white = "rgb(255,236,214)";
