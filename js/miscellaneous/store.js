let save = {};

resetSave();

function resetSave() {
    save = {
        grid: {
            offsetX: 390,
            offsetY: 90,
            sizeX: 800,
            sizeY: 600,
            cell: {
                sizeX: 200,
                sizeY: 200,
            }
        },
        cards: {
            discoverySpeed: 0.005,
            instantDiscovery: false,
            discoveryMultiplier: 4,
            degenSpeed: 0.01,
            infoQuantity: 1,
        },
        currency: {
            money: 0,
            matchEarnings: 5,
            stars: 0,
            starMultiplier: 2,
            numberOfMatches: 0,
            allTimeEarnings: 0,
        },
        people: {
            potentialPool: 50,
            weakCouples: 0,
            definitiveCouples: 0,
            allTimeCouple: 0,
        },
        marketing: {
            unlocked: false,
            instant: {
                cost: 0,
                quantity: 10,
                speed: 1,
                flavorText: "Tell everyone about this app",
                active: false,
                unlocked: false,
            },
            progress: {
                cost: 50,
                quantity: 1,
                speed: 5,
                flavorText: "Free emojis for all new inscription !",
                active: false,
                unlocked: false,
            },
            perma: {
                cost: 2,
                quantity: 1,
                speed: 5,
                flavorText: "Advertise everywhere possible",
                active: false,
                unlocked: false,
            },
        },
        ai: {
            unlocked: false,
            speed: 0.5,
            quantity: 1,
            notationMin: -10,
            notationMax: 4,
            hoverUnlocked: false,
        },
        research: {
            ratio: 20,
            bonusMax: 0,
        },
    
        // title, tooltip, cost, advancement, bought, effect, moneyPerTick
        upgrades: [
            ["Match Earnings", ["Augment the price for the customer", "+ 1$/match"], 10, 0, false, "moneyMatch1", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 2$/match"], 50, 0, false, "moneyMatch2", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 7$/match"], 150, 0, false, "moneyMatch3", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 10$/match"], 300, 0, false, "moneyMatch4", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 25$/match"], 750, 0, false, "moneyMatch5", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 50$/match"], 1500, 0, false, "moneyMatch6", 0],
            ["Match Earnings", ["Augment the price for the customer", "+ 100$/match"], 4800, 0, false, "moneyMatch7", 0],["Match Earnings", ["Augment the price for the customer", "+ 400$/match"], 13200, 0, false, "moneyMatch8", 0],

            ["Profile Speed", ["Push the customers to complete their profile faster", "profile speed x2"], 10, 0, false, "discoverySpeed", 0],
            ["Profile Speed", ["Push the customers to complete their profile faster", "profile speed x2"], 75, 0, false, "discoverySpeed", 0],
            ["Profile Speed", ["Push the customers to complete their profile faster", "profile speed x2"], 200, 0, false, "discoverySpeed", 0],
            ["Profile Speed", ["Push the customers to complete their profile faster", "profile speed x2"], 500, 0, false, "discoverySpeed", 0],
            ["Instant Profile", ["Customers complete their profile first thing when installing", "Profile instantly completed."], 1600, 0, false, "instantDiscovery", 0],

            ["More Patience", ["People get bored slowlier, and will uninstall later", "Uninstall Speed x1/2"], 10, 0, false, "degen", 0],
            ["More Patience", ["People get bored slowlier, and will uninstall later", "Uninstall Speed x1/2"], 50, 0, false, "degen", 0],
            ["More Patience", ["People get bored slowlier, and will uninstall later", "Uninstall Speed x1/2"], 200, 0, false, "degen", 0],
            ["Infinite Patience", ["Add a splash screen of cats so people never get bored", "No more uninstalls"], 1100, 0, false, "noDegen", 0],

            ["Match Infos", ["Hire an army of interns to compile data for you", "Unlock match notation on hover"], 500, 0, false, "matchHover", 0],
            ["Auto Match", ["Train an A.I. to match automatically customers", "Automatch: range from terrible to fair"], 250, 0, false, "matchAuto", 0],

            ["Match Speed", ["Train your A.I. on correspondance columns of love magazines", "Auto Match Speed x2"], 600, 0, false, "matchSpeed", 0],
            ["Match Speed", ["Train your A.I. on other apps", "Auto Match Speed x2"], 1800, 0, false, "matchSpeed", 0],
            ["Match Speed", ["Train your A.I. on dota", "Auto Match Speed x2"], 3400, 0, false, "matchSpeed", 0],
            ["Match Speed", ["Train your A.I. on internet quizzes", "Auto Match Speed x2"], 5700, 0, false, "matchSpeed", 0],
            ["Match Speed", ["Train your A.I. on telenovelas", "Auto Match Speed x2"], 8700, 0, false, "matchSpeed", 0],

            ["Match Quality", ["Refine your A.I. to get better matches", "Match range from terrible to good"], 1100, 0, false, "matchQuality1", 0],
            ["Match Quality", ["Refine your A.I. to get better matches", "Match range from mediocre to excellent"], 2000, 0, false, "matchQuality2", 0],
            ["Match Quality", ["Refine your A.I. to get better matches", "Match range from fair to outstanding"], 3000, 0, false, "matchQuality3", 0],
            ["Match Quality", ["Refine your A.I. to get better matches", "Match range from good to perfect"], 5000, 0, false, "matchQuality4", 0],

            ["Match Quantity", ["Auto matches come now in bulk", "Match auto +1"], 6800, 0, false, "matchQuantity", 0],
            ["Match Quantity", ["Auto matches come now in bulk", "Match auto +1"], 10400, 0, false, "matchQuantity", 0],
            ["Match Quantity", ["Auto matches come now in bulk", "Match auto +1"], 15700, 0, false, "matchQuantity", 0],["Match Quantity", ["Auto matches come now in bulk", "Match auto +1"], 22000, 0, false, "matchQuantity", 0],

            ["Storage Size", ["Deal with more customers at the same time", "Board size increase"], 1200, 0, false, "boardSize2", 0],
            ["Storage Size", ["Deal with more customers at the same time", "Board size increase"], 12000, 0, false, "boardSize3", 0],
            ["Storage Size", ["Deal with more customers at the same time", "Board size increase"], 24000, 0, false, "boardSize4", 0],

            ["Increase Popularity", ["Add \"Premium\" to the app title, people will come in flock", "New user cooldown x1/2"], 700, 0, false, "starMultx2", 0],
            ["Increase Popularity", ["Add \"Original\" to the app title, people will come in flock", "New user cooldown x1/2"], 3400, 0, false, "starMultx2", 0],
            ["Increase Popularity", ["Add \"Verified\" to the app title, people will come in flock", "New user cooldown x1/2"], 12500, 0, false, "starMultx2", 0],

            ["Word of Mouth", ["Advertise on relationship advices forums", "Unlock a new type of marketing"], 10, 0, false, "marketInstant", 0],
            ["Guerilla Marketing", ["Tag the name of the app in the toilets of every bars", "Unlock a new type of marketing"], 200, 0, false, "marketProgress", 0],
            ["Growth Marketing", ["Constantly carry advertising everywhere", "Unlock a new type of marketing"], 2000, 0, false, "marketPerma", 0],

            ["Word of Mouth", ["Diversify: pay a blogger to write about love", "Word of Mouth x2"], 75, 0, false, "marketInstantx2", 0],
            ["Word of Mouth", ["Diversify: pay a dozen of bloggers to write about love", "Word of Mouth x5"], 300, 0, false, "marketInstantx5", 0],
            ["Word of Mouth", ["Diversify: pay an army of bloggers to write about love", "Word of Mouth x5"], 2900, 0, false, "marketInstantx5", 0],

            ["Guerilla Marketing", ["Add \"illimited matches\" as description", "Guerilla Marketing cost x2 and duration x5"], 1300, 0, false, "marketProgressx2", 0],
            ["Guerilla Marketing", ["Add \"love guaranteed\" as description", "Guerilla Marketing cost x2 and duration x5"], 4500, 0, false, "marketProgressx2", 0],

            ["Growth Marketing", ["Add advertisements on every other apps", "Growth Marketing +4 potential pool"], 4400, 0, false, "marketPerma2", 0],
            ["Growth Marketing", ["Get your own spot on national television", "Growth Marketing +15 potential pool"], 9400, 0, false, "marketPerma3", 0],
            ["Growth Marketing", ["Advertise on every OnlyFans", "Growth Marketing +80 potential pool"], 13200, 0, false, "marketPerma4", 0],

            ["More Infos", ["We need more informations on profiles to create better matches", "+ 1 info"], 50, 0, false, "info+", 0],
            ["More Infos", ["We need more informations on profiles to create better matches", "+ 1 info"], 90, 0, false, "info+", 0],
            ["All Infos", ["We need more informations on profiles to create better matches", "+ 3-7 info"], 250, 0, false, "info+", 0],

            ["New App", ["Change the name of the app, resetting the notation", "Researching this will lock \"Sell Data\" research"], 8000, 0, false, "noteReset", 0],
            ["Sell Data", ["Sell your customers data : +25$/all time matches", "Researching this will lock \"New App\" research"], 8000, 0, false, "sellData", 0],

            ["Lab Upgrade", ["Promote your interns to senior Researcher", "+10 max research"], 800, 0, false, "labMax1", 0],
            ["Lab Upgrade", ["Promote your senior Researchers to VIPhd", "+30 max research"], 1900, 0, false, "labMax2", 0],
            ["Lab Upgrade", ["Accelerate your research potential", "-1 couples needed for max Research"], 3200, 0, false, "labMax3", 0],
            ["Lab Upgrade", ["Accelerate your research potential", "-1 couples needed for max Research"], 7100, 0, false, "labMax3", 0],

            ["Max Interns", ["Recrute every interns possible in the country", "+100 max research (will lock \"Accelerate Research\")"], 1900, 0, false, "maxInterns", 0],
            ["Accelerate Research", ["Believe in the power of love", "-5 couples needed for max Research (will lock \"Max Interns\")"], 1900, 0, false, "accelerateResearch", 0],

            ["Get Bought", ["Spend every dime possible to prove this app is profitable", "Win"], 1000000, 0, false, "win", 0],
        ],
    };
}
