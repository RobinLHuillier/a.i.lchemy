const lastNames = [
    'SMITH',
    'JOHNSON',
    'WILLIAMS',
    'BROWN',
    'JONES',
    'GARCIA',
    'MILLER',
    'DAVIS',
    'RODRIGUEZ',
    'MARTINEZ',
    'HERNANDEZ',
    'LOPEZ',
    'GONZALEZ',
    'WILSON',
    'ANDERSON',
    'THOMAS',
    'TAYLOR',
    'MOORE',
    'JACKSON',
    'MARTIN',
    'LEE',
    'PEREZ',
    'THOMPSON',
    'WHITE',
    'HARRIS',
    'SANCHEZ',
    'CLARK',
    'RAMIREZ',
    'LEWIS',
    'ROBINSON',
    'WALKER',
    'YOUNG',
    'ALLEN',
    'KING',
    'WRIGHT',
    'SCOTT',
    'TORRES',
    'NGUYEN',
    'HILL',
    'FLORES',
    'GREEN',
    'ADAMS',
    'NELSON',
    'BAKER',
    'HALL',
    'RIVERA',
    'CAMPBELL',
    'MITCHELL',
    'CARTER',
    'ROBERTS',
    'GOMEZ',
    'PHILLIPS',
    'EVANS',
    'TURNER',
    'DIAZ',
    'PARKER',
    'CRUZ',
    'EDWARDS',
    'COLLINS',
    'REYES',
    'STEWART',
    'MORRIS',
    'MORALES',
    'MURPHY',
    'COOK',
    'ROGERS',
    'GUTIERREZ',
    'ORTIZ',
    'MORGAN',
    'COOPER',
    'PETERSON',
    'BAILEY',
    'REED',
    'KELLY',
    'HOWARD',
    'RAMOS',
    'KIM',
    'COX',
    'WARD',
    'RICHARDSON',
    'WATSON',
    'BROOKS',
    'CHAVEZ',
    'WOOD',
    'JAMES',
    'BENNETT',
    'GRAY',
    'MENDOZA',
    'RUIZ',
    'HUGHES',
    'PRICE',
    'ALVAREZ',
    'CASTILLO',
    'SANDERS',
    'PATEL',
    'MYERS',
    'LONG',
    'ROSS',
    'FOSTER',
    'JIMENEZ',
    'POWELL',
    'JENKINS',
    'PERRY',
    'RUSSELL',
    'SULLIVAN',
    'BELL',
    'COLEMAN',
    'BUTLER',
    'HENDERSON',
    'BARNES',
    'GONZALES',
    'FISHER',
    'VASQUEZ',
    'SIMMONS',
    'ROMERO',
    'JORDAN',
    'PATTERSON',
    'ALEXANDER',
    'HAMILTON',
    'GRAHAM',
    'REYNOLDS',
    'GRIFFIN',
    'WALLACE',
    'MORENO',
    'WEST',
    'COLE',
    'HAYES',
    'BRYANT',
    'HERRERA',
    'GIBSON',
    'ELLIS',
    'TRAN',
    'MEDINA',
    'AGUILAR',
    'STEVENS',
    'MURRAY',
    'FORD',
    'CASTRO',
    'MARSHALL',
    'OWENS',
    'HARRISON',
    'FERNANDEZ',
    'MCDONALD',
    'WOODS',
    'WASHINGTON',
    'KENNEDY',
    'WELLS',
    'VARGAS',
    'HENRY',
    'CHEN',
    'FREEMAN',
    'WEBB',
    'TUCKER',
    'GUZMAN',
    'BURNS',
    'CRAWFORD',
    'OLSON',
    'SIMPSON',
    'PORTER',
    'HUNTER',
    'GORDON',
    'MENDEZ',
    'SILVA',
    'SHAW',
    'SNYDER',
    'MASON',
    'DIXON',
    'MUNOZ',
    'HUNT',
    'HICKS',
    'HOLMES',
    'PALMER',
    'WAGNER',
    'BLACK',
    'ROBERTSON',
    'BOYD',
    'ROSE',
    'STONE',
    'SALAZAR',
    'FOX',
    'WARREN',
    'MILLS',
    'MEYER',
    'RICE',
    'SCHMIDT',
    'GARZA',
    'DANIELS',
    'FERGUSON',
    'NICHOLS',
    'STEPHENS',
    'SOTO',
    'WEAVER',
    'RYAN',
    'GARDNER',
    'PAYNE',
    'GRANT',
    'DUNN',
    'KELLEY',
    'SPENCER',
    'HAWKINS',
    'ARNOLD',
    'PIERCE',
    'VAZQUEZ',
    'HANSEN',
    'PETERS',
    'SANTOS',
    'HART',
    'BRADLEY',
    'KNIGHT',
    'ELLIOTT',
    'CUNNINGHAM',
    'DUNCAN',
    'ARMSTRONG',
    'HUDSON',
    'CARROLL',
    'LANE',
    'RILEY',
    'ANDREWS',
    'ALVARADO',
    'RAY',
    'DELGADO',
    'BERRY',
    'PERKINS',
    'HOFFMAN',
    'JOHNSTON',
    'MATTHEWS',
    'PENA',
    'RICHARDS',
    'CONTRERAS',
    'WILLIS',
    'CARPENTER',
    'LAWRENCE',
    'SANDOVAL',
    'GUERRERO',
    'GEORGE',
    'CHAPMAN',
    'RIOS',
    'ESTRADA',
    'ORTEGA',
    'WATKINS',
    'GREENE',
    'NUNEZ',
    'WHEELER',
    'VALDEZ',
    'HARPER',
    'BURKE',
    'LARSON',
    'SANTIAGO',
    'MALDONADO',
    'MORRISON',
];

const firstNamesFemale = [
    'MARY',
    'PATRICIA',
    'LINDA',
    'BARBARA',
    'ELIZABETH',
    'JENNIFER',
    'MARIA',
    'SUSAN',
    'MARGARET',
    'DOROTHY',
    'LISA',
    'NANCY',
    'KAREN',
    'BETTY',
    'HELEN',
    'SANDRA',
    'DONNA',
    'CAROL',
    'RUTH',
    'SHARON',
    'MICHELLE',
    'LAURA',
    'SARAH',
    'KIMBERLY',
    'DEBORAH',
    'JESSICA',
    'SHIRLEY',
    'CYNTHIA',
    'ANGELA',
    'MELISSA',
    'BRENDA',
    'AMY',
    'ANNA',
    'REBECCA',
    'VIRGINIA',
    'KATHLEEN',
    'PAMELA',
    'MARTHA',
    'DEBRA',
    'AMANDA',
    'STEPHANIE',
    'CAROLYN',
    'CHRISTINE',
    'MARIE',
    'JANET',
    'CATHERINE',
    'FRANCES',
    'ANN',
    'JOYCE',
    'DIANE',
    'ALICE',
    'JULIE',
    'HEATHER',
    'TERESA',
    'DORIS',
    'GLORIA',
    'EVELYN',
    'JEAN',
    'CHERYL',
    'MILDRED',
    'KATHERINE',
    'JOAN',
    'ASHLEY',
    'JUDITH',
    'ROSE',
    'JANICE',
    'KELLY',
    'NICOLE',
    'JUDY',
    'CHRISTINA',
    'KATHY',
    'THERESA',
    'BEVERLY',
    'DENISE',
    'TAMMY',
    'IRENE',
    'JANE',
    'LORI',
    'RACHEL',
    'MARILYN',
    'ANDREA',
    'KATHRYN',
    'LOUISE',
    'SARA',
    'ANNE',
    'JACQUELINE',
    'WANDA',
    'BONNIE',
    'JULIA',
    'RUBY',
    'LOIS',
    'TINA',
    'PHYLLIS',
    'NORMA',
    'PAULA',
    'DIANA',
    'ANNIE',
    'LILLIAN',
    'EMILY',
    'ROBIN',
    'PEGGY',
    'CRYSTAL',
    'GLADYS',
    'RITA',
    'DAWN',
    'CONNIE',
    'FLORENCE',
    'TRACY',
    'EDNA',
    'TIFFANY',
    'CARMEN',
    'ROSA',
    'CINDY',
    'GRACE',
    'WENDY',
    'VICTORIA',
    'EDITH',
    'KIM',
    'SHERRY',
    'SYLVIA',
    'JOSEPHINE',
    'THELMA',
    'SHANNON',
    'SHEILA',
    'ETHEL',
    'ELLEN',
    'ELAINE',
    'MARJORIE',
    'CARRIE',
    'CHARLOTTE',
    'MONICA',
    'ESTHER',
    'PAULINE',
    'EMMA',
    'JUANITA',
    'ANITA',
    'RHONDA',
    'HAZEL',
    'AMBER',
    'EVA',
    'DEBBIE',
    'APRIL',
    'LESLIE',
    'CLARA',
    'LUCILLE',
    'JAMIE',
    'JOANNE',
    'ELEANOR',
    'VALERIE',
    'DANIELLE',
    'MEGAN',
    'ALICIA',
    'SUZANNE',
    'MICHELE',
    'GAIL',
    'BERTHA',
    'DARLENE',
    'VERONICA',
    'JILL',
    'ERIN',
    'GERALDINE',
    'LAUREN',
    'CATHY',
    'JOANN',
    'LORRAINE',
    'LYNN',
    'SALLY',
    'REGINA',
    'ERICA',
    'BEATRICE',
    'DOLORES',
    'BERNICE',
    'AUDREY',
    'YVONNE',
    'ANNETTE',
    'JUNE',
    'SAMANTHA',
    'MARION',
    'DANA',
    'STACY',
    'ANA',
    'RENEE',
    'IDA',
    'VIVIAN',
    'ROBERTA',
    'HOLLY',
    'BRITTANY',
    'MELANIE',
    'LORETTA',
    'YOLANDA',
    'JEANETTE',
    'LAURIE',
    'KATIE',
    'KRISTEN',
    'VANESSA',
    'ALMA',
    'SUE',
    'ELSIE',
    'BETH',
    'JEANNE',
];

const firstNamesMale = [
    'JAMES',
    'JOHN',
    'ROBERT',
    'MICHAEL',
    'WILLIAM',
    'DAVID',
    'RICHARD',
    'CHARLES',
    'JOSEPH',
    'THOMAS',
    'CHRISTOPHER',
    'DANIEL',
    'PAUL',
    'MARK',
    'DONALD',
    'GEORGE',
    'KENNETH',
    'STEVEN',
    'EDWARD',
    'BRIAN',
    'RONALD',
    'ANTHONY',
    'KEVIN',
    'JASON',
    'MATTHEW',
    'GARY',
    'TIMOTHY',
    'JOSE',
    'LARRY',
    'JEFFREY',
    'FRANK',
    'SCOTT',
    'ERIC',
    'STEPHEN',
    'ANDREW',
    'RAYMOND',
    'GREGORY',
    'JOSHUA',
    'JERRY',
    'DENNIS',
    'WALTER',
    'PATRICK',
    'PETER',
    'HAROLD',
    'DOUGLAS',
    'HENRY',
    'CARL',
    'ARTHUR',
    'RYAN',
    'ROGER',
    'JOE',
    'JUAN',
    'JACK',
    'ALBERT',
    'JONATHAN',
    'JUSTIN',
    'TERRY',
    'GERALD',
    'KEITH',
    'SAMUEL',
    'WILLIE',
    'RALPH',
    'LAWRENCE',
    'NICHOLAS',
    'ROY',
    'BENJAMIN',
    'BRUCE',
    'BRANDON',
    'ADAM',
    'HARRY',
    'FRED',
    'WAYNE',
    'BILLY',
    'STEVE',
    'LOUIS',
    'JEREMY',
    'AARON',
    'RANDY',
    'HOWARD',
    'EUGENE',
    'CARLOS',
    'RUSSELL',
    'BOBBY',
    'VICTOR',
    'MARTIN',
    'ERNEST',
    'PHILLIP',
    'TODD',
    'JESSE',
    'CRAIG',
    'ALAN',
    'SHAWN',
    'CLARENCE',
    'SEAN',
    'PHILIP',
    'CHRIS',
    'JOHNNY',
    'EARL',
    'JIMMY',
    'ANTONIO',
    'DANNY',
    'BRYAN',
    'TONY',
    'LUIS',
    'MIKE',
    'STANLEY',
    'LEONARD',
    'NATHAN',
    'DALE',
    'MANUEL',
    'RODNEY',
    'CURTIS',
    'NORMAN',
    'ALLEN',
    'MARVIN',
    'VINCENT',
    'GLENN',
    'JEFFERY',
    'TRAVIS',
    'JEFF',
    'CHAD',
    'JACOB',
    'LEE',
    'MELVIN',
    'ALFRED',
    'KYLE',
    'FRANCIS',
    'BRADLEY',
    'JESUS',
    'HERBERT',
    'FREDERICK',
    'RAY',
    'JOEL',
    'EDWIN',
    'DON',
    'EDDIE',
    'RICKY',
    'TROY',
    'RANDALL',
    'BARRY',
    'ALEXANDER',
    'BERNARD',
    'MARIO',
    'LEROY',
    'FRANCISCO',
    'MARCUS',
    'MICHEAL',
    'THEODORE',
    'CLIFFORD',
    'MIGUEL',
    'OSCAR',
    'JAY',
    'JIM',
    'TOM',
    'CALVIN',
    'ALEX',
    'JON',
    'RONNIE',
    'BILL',
    'LLOYD',
    'TOMMY',
    'LEON',
    'DEREK',
    'WARREN',
    'DARRELL',
    'JEROME',
    'FLOYD',
    'LEO',
    'ALVIN',
    'TIM',
    'WESLEY',
    'GORDON',
    'DEAN',
    'GREG',
    'JORGE',
    'DUSTIN',
    'PEDRO',
    'DERRICK',
    'DAN',
    'LEWIS',
    'ZACHARY',
    'COREY',
    'HERMAN',
    'MAURICE',
    'VERNON',
    'ROBERTO',
    'CLYDE',
    'GLEN',
    'HECTOR',
    'SHANE',
    'RICARDO',
    'SAM',
    'RICK',
    'LESTER',
    'BRENT',
    'RAMON',
    'CHARLIE',
    'TYLER',
    'GILBERT',
    'GENE',
];

function randFirstName(sex) {
    const e = Math.random();
    if (sex === "male" || (sex === "non-binary" && e < 0.5)) {
        return randElem(firstNamesMale);
    }
    return randElem(firstNamesFemale);
}

function randLastName() {
    return randElem(lastNames);
}

function randGender() {
    const e = Math.random();
    if (e < 0.49) {
        return "male";
    }
    if (e < 0.98) {
        return "female";
    }
    return "non-binary";
}

function randSexuality(gender) {
    const e = Math.random();
    if (e < 0.80) {
        if ("male" === gender) {
            return "female";
        }
        return "male";
    }
    if (e < 0.85) {
        return "male and female";
    }
    if (e < 0.95) {
        return gender;
    }
    return "don't care";
}

function rangeForAge(age) {
    const e = Math.random();
    if (age < 25) {
        return [18, 30];
    }
    if (age <= 30) {
        if (e < 0.5) {
            return [18, 30];
        }
        return [25,40];
    }
    if (age <= 40) {
        if (e < 0.5) {
            return [25, 40];
        }
        return [30,45];
    }
    if (age <= 45) {
        if (e < 0.5) {
            return [30, 45];
        }
        return [40,55];
    }
    if (age <= 50) {
        return [40,55];
    }
    if (age <= 55) {
        if (e < 0.5) {
            return [40, 55];
        }
        return [50,65];
    }
    if (age <= 65) {
        return [50,65];
    }
    return [65,100];
}

function randAge() {
    const e = Math.random();
    if (e < 0.5) {
        return rand(30,18);
    }
    if (e < 0.75) {
        return rand(45,30);
    }
    return rand(95, 18);
}

function randAgeRange(age) {
    const e = Math.random();
    if (e < 0.7) {
        return rangeForAge(age);
    }
    return rangeForAge(randAge());
}

function randTypeRelation() {
    return randElem(["serious", "fun"]);
}

function randUnitLikeDislike() {
    return randElem([
        "cinema",
        "hunting",
        "sports",
        "adventure",
        "dreaming",
        "reading",
        "music",
        "volunteering",
        "garden",
        "dance",
        "theater",
        "video games",
        "writing",
        "museums",
        "tinkering",
        "woodworking",
        "driving",
        "singing",
    ]);
}

function randLikeDislike(likes = []) {
    let items = []
    for (let i=0; i<rand(4,1); i++) {
        let item = randUnitLikeDislike();
        while (items.includes(item) || likes.includes(item)) {
            item = randUnitLikeDislike();
        }
        items.push(item);
    }
    return items;
}

function compareGenderAndSexuality(card1, card2) {
    return (
        (card1.gender === card2.sexuality && card1.sexuality === card2.gender) ||
        (card1.sexuality === "don't care" && card2.sexuality === "don't care") ||
        (card1.sexuality === "don't care" && card2.sexuality === card2.gender) ||
        (card1.sexuality === card2.gender && card2.sexuality === "don't care") ||
        (card1.sexuality === "male and female" && card2.gender !== "non-binary" && card2.sexuality === card2.gender) ||
        (card1.sexuality === card2.gender && card1.gender !== "non-binary" && card2.sexuality === "male and female")
    );
}

function isAgeInRange(age, range) {
    return age >= range[0] && age <= range[1];
}

function compareAge(card1, card2) {
    return isAgeInRange(card1.age, card2.ageRange) && isAgeInRange(card2.age, card1.ageRange);
}

function compareTypeRelation(card1, card2) {
    return card1.typeRelation === card2.typeRelation;
}

function compareLikesInDislikes(card1, card2) {
    let count = 0;
    card1.like.forEach(like => {
        if (card2.dislike.includes(like)) {
            count++;
        }
    })
    card2.like.forEach(like => {
        if (card1.dislike.includes(like)) {
            count++;
        }
    })
    return count;
}

function compareLikesInLikesAndDislikesInDislikes(card1, card2) {
    let count = 0;
    card1.like.forEach(like => {
        if (card2.like.includes(like)) {
            count++;
        }
    })
    card1.dislike.forEach(dislike => {
        if (card2.dislike.includes(dislike)) {
            count++;
        }
    })
    return count;
}