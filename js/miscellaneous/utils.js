function rand(max, min) {
    if (undefined === min) {
        min = 0;
    }
    return Math.floor(Math.random() * (max-min)) + min;
}

function randElem(array) {
    return array[rand(array.length)];
}

function addFade(color, fade) {
    return color.slice(0,-1) + "," + fade.toString() + ")";
}

function delElem(array, elem) {
    if (array.indexOf(elem) === -1) {
        console.error("[delElem] trying to delete an element non existent", array, elem);
        return;
    }
    array.splice(array.indexOf(elem), 1);
}

function strokeMenuRect(x,y,w,h,ctx,fill=false,colorFill=white,fade=0) {
    ctx.strokeStyle = blue;
    ctx.lineWidth = 2;
    if (fill) {
        ctx.fillStyle = addFade(colorFill,fade);
        ctx.fillRect(x+4, y+4, w-8, h-8);
    }
    ctx.strokeRect(x+4, y+4, w-8, h-8);
    ctx.strokeRect(x, y, w, h);
    ctx.strokeStyle = gold;
    ctx.strokeRect(x+2, y+2, w-4, h-4);
}

function roundRect(ctx, x, y, width, height, colorStroke, colorFill, radius = 5) {
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.arcTo(x + width, y, x + width, y + radius, radius);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.arcTo(x + width, y + height, x + width - radius, y + height, radius);
    ctx.lineTo(x + radius, y + height);
    ctx.arcTo(x, y + height, x, y + height - radius, radius);
    ctx.lineTo(x, y + radius);
    ctx.arcTo(x, y, x + radius, y, radius);
    ctx.closePath();
    ctx.strokeStyle = colorStroke;
    ctx.fillStyle = colorFill;
    ctx.fill();
    ctx.stroke();
}

function formatNumber(num) {
    if (num < 10000) {
        return Math.floor(num).toString();
    }
    let power = 4;
    num /= 10000;
    while (num >= 10) {
        power++;
        num /= 10;
    }
    return (Math.floor(100*num)/100).toString() + "e" + power.toString();
}

function shuffle(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
}

function pointsToNotation(points) {
    let superlatif = "";
    let textSizeMult = 1;
    let noteStars = 0;
    if (points < -90) {
        superlatif = "execrable";
        textSizeMult = 0.8;
    } else if (points < 0) {
        superlatif = "terrible";
        noteStars = 1;
    } else if (points < 2) {
        superlatif = "mediocre";
        textSizeMult = 1.1;
        noteStars = 2;
    } else if (points < 4) {
        superlatif = "fair";
        textSizeMult = 1.2;
        noteStars = 3;
    } else if (points < 7) {
        superlatif = "good";
        textSizeMult = 1.3;
        noteStars = 4;
    } else if (points < 10) {
        superlatif = "excellent";
        textSizeMult = 1.4;
        noteStars = 4;
    } else if (points < 13) {
        superlatif = "outstanding";
        textSizeMult = 1.5;
        noteStars = 5;
    } else {
        superlatif = "perfect";
        textSizeMult = 1.6;
        noteStars = 5;
    }
    const msg = superlatif + " " + "match";

    return [msg, textSizeMult, noteStars, superlatif];
}

function serializePosition(x,y) {
    return x.toString() + "-" + y.toString();
}

function unserializePosition(position) {
    if (position === undefined) {
        console.error("[unserializePosition] position is undefined");
        return;
    }
    if (position.indexOf("-") === -1) {
        console.error("[unserializePosition] position is not serialized");
        return;
    }
    return [Number(position.slice(0,position.indexOf("-"))), Number(position.slice(position.indexOf("-")+1))];
}

function drawLine(ctx, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

function ticksToTime(tick) {
    const sec = tick/tickPerSec;
    const min = sec/60;
    const hours = min/60;
    const days = hours/24;
    if (min < 1) {
        return (Math.floor(sec*10)/10).toString()+"s";
    }
    if (hours < 1) {
        return (Math.floor(min*100)/100).toString()+"m";
    }
    if (days < 1) {
        return (Math.floor(hours*100)/100).toString()+"h";
    }
    return (Math.floor(days*100)/100).toString()+"d";
}